# Demo shop Test Framework

## Technology
- Java 19
- TestNg
- Allure Reports
- Selenide
- Apache maven


### Description
#### This is a framework meant to test a demo shop application.

##### The project consists of 24 main classes, 1 configuration class, 9 data provider classes and 11 test classes.

##### The project contains tests about:
- Authentication
- Page navigation
- Search bar
- Product sorting
- User and products interaction
- Wishlist page


##### The total number of tests is:
- 769
#### Test passed
- 653
#### Test failed
- 116
### project report example:
![](report1.PNG)
