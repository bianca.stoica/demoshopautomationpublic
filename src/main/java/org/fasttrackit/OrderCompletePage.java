package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class OrderCompletePage {

    private final SelenideElement completeOrderMessage = $(".text-center");
    private final SelenideElement continueShoppingButton = $(".btn-success");

    public boolean completeOrderMessageIsDisplayed() {
        return completeOrderMessage.isDisplayed();
    }

    public void clickOnTheContinueShoppingButton() {
        continueShoppingButton.click();
    }
}
