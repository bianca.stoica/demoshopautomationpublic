package org.fasttrackit;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import java.util.List;
import java.util.stream.Collectors;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class SearchPage {

    private final SelenideElement searchBox = $("#input-search");
    private final SelenideElement searchButton = $(".btn-light");
    private final ElementsCollection searchResults = $$(".card-link");

    public void clickOnTheSearchBox() {
        System.out.println("Click on the Search Box.");
        searchBox.click();
    }

    public void typeInKeyword(String inputtedWord) {
        System.out.println("Type in " + inputtedWord);
        searchBox.setValue(inputtedWord);
    }

    public void clickOnTheSearchButton() {
        System.out.println("Click on the Search Button.");
        searchButton.click();
    }

    public boolean areSearchResultsContainingKeyword(String keyword) {
        System.out.println("Verify search results contain the keyword: " + keyword);
        List<String> resultTexts = searchResults.texts();
        return resultTexts.stream()
                .anyMatch(text -> text.toLowerCase().contains(keyword.toLowerCase()));
    }

    private ElementsCollection productPrices() {
        return $$(".card-footer .card-text span");
    }

    public boolean areSearchResultsContainingPrice(double targetPrice) {
        System.out.println("Verify search results contain the price: " + targetPrice);
        List<Double> resultPrices = productPrices().texts().stream()
                .map(Double::parseDouble)
                .collect(Collectors.toList());

        return resultPrices.stream()
                .anyMatch(price -> Math.abs(price - targetPrice) < 0.01); // Adjust tolerance as needed
    }

    private boolean isSearchBarDisplayed(){
        return searchBox.exists();
    }
}


