package org.fasttrackit;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class WishlistPage {
    private final ElementsCollection wishlistProducts = $$(".card-link");
    private final SelenideElement productName = $(".card-link");
    private final SelenideElement addToCartButton = $(".fa-cart-plus");

    public boolean isWishlistPageEmpty() {
        return wishlistProducts.isEmpty();
    }

    public void clickOnTheProductName() {
        productName.click();
    }

    public void clickOnTheCartButton() {
        addToCartButton.click();
    }

}
