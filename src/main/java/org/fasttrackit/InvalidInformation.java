package org.fasttrackit;

public class InvalidInformation extends YourInformation{
    public InvalidInformation(String firstName, String lastName, String address) {
        super(firstName, lastName, address);
    }
}
