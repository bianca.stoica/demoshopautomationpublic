package org.fasttrackit;

public class ValidAccount extends Account {
    private final String greetingMsg;

    public ValidAccount(String user, String password) {
        super(user, password);
        this.greetingMsg = "Hi " + user + "!";
    }

    public String getGreetingMsg() {
        return greetingMsg;
    }
}
