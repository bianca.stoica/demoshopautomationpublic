package org.fasttrackit;

public class InvalidSearchQuery extends SearchQuery {
    public InvalidSearchQuery(String searchQuery) {
        super(searchQuery);
    }
}
