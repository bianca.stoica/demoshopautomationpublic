package org.fasttrackit;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class CartItem {
    private final SelenideElement row;
    private final SelenideElement reduceAmount;
    private final SelenideElement increaseAmount;
    private final SelenideElement amountControlSection;
    private final SelenideElement trashCanButton;
    private final SelenideElement amountControlPrice;
    private final SelenideElement productName = $("[id^='item_'][id$='_title_link']");
    private final SelenideElement productPrice = $("[style='width: 100px;']");


    public CartItem(String productId) {
        String itemById = String.format("item_%s_title_link", productId);
        SelenideElement item = $(By.id(itemById));
        this.row = item.parent().parent();
        this.reduceAmount = row.$(".fa-minus-circle");
        this.increaseAmount = row.$(".fa-plus-circle");
        this.amountControlSection = this.increaseAmount.parent().parent();
        this.trashCanButton = row.$(".fa-trash");
        this.amountControlPrice = $(".container .row .col-md-auto:nth-child(3) div");
    }

    public void increaseAmount() {
        System.out.println("Click on the increase amount button.");
        this.increaseAmount.click();
    }

    public void reduceAmount() {
        System.out.println("Click on the reduce amount button.");
        this.reduceAmount.click();
    }

    public String getItemAmount() {
        return this.amountControlSection.text();

    }

    public void clickTrashCanButton() {
        System.out.println("Click the trash can button");
        trashCanButton.click();
    }

    public String getAmountControlPrice() {
        return this.amountControlPrice.text();
    }

    public String getProductName() {
        return this.productName.text();
    }

    public String getProductPrice() {
        return this.productPrice.text();
    }

    double parsePriceString(String priceString) {
        return Double.parseDouble(priceString.replaceAll("[^0-9.]+", ""));
    }
}
