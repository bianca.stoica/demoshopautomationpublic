package org.fasttrackit;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class CartPage {
    private final SelenideElement emptyCartPageElement = $(".text-center");
    private final SelenideElement checkoutButton = $(".btn-success");
    private final SelenideElement totalAmount = $(".amount-total");
    private final SelenideElement continueShoppingButton = $(".btn-danger");


    public CartPage() {
    }

    public String getEmptyCartPageText() {
        return this.emptyCartPageElement.text();
    }

    public boolean isEmptyCartMessageDisplayed() {
        return emptyCartPageElement.isDisplayed();
    }

    public void clickOnTheCheckoutButton() {
        System.out.println("Click on the Checkout button");
        checkoutButton.click();
    }

    public void clickOnTheContinueShoppingButton() {
        System.out.println("Click on the Continue shopping button");
        continueShoppingButton.click();
    }

    public int getProductCount() {
        ElementsCollection productItems = $$("div.col a[id^='item_'][id$='_title_link']");
        return productItems.size();
    }

    public double getTotalAmount() {
        String totalAmountText = totalAmount.text();
        String numericPart = totalAmountText.replaceAll("[^\\d.]+", "");
        return Double.parseDouble(numericPart);
    }

    public boolean isProductPresentInCart(String productName) {
        String productSelector = String.format("div.col a[id^='item_%s_title_link']", productName);
        SelenideElement productElement = $(productSelector);
        return productElement.exists();
    }
}





