package org.fasttrackit;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;


public class ProductCards {
    private final ElementsCollection cards = $$(".card");
    private final SelenideElement sortButton = $(".sort-products-select");
    private final SelenideElement sortAZ = $("option[value=az]");
    private final SelenideElement sortZA = $("option[value=za]");
    private final SelenideElement sortHiLow = $("option[value=hilo");
    private final SelenideElement sortLowHi = $("option[value=lohi");
    private final ElementsCollection productLinks = $$(".card-link");


    public ProductCards() {

    }

    public void clickOnTheSortButton() {
        System.out.println("Click on the sort button.");
        this.sortButton.click();
    }

    public void clickOnTheAZSortButton() {
        System.out.println("Click on the AZ sort button.");
        this.sortAZ.click();
    }

    public void clickOnTheZASortButton() {
        System.out.println("Click on the ZA sort button.");
        this.sortZA.click();
    }


    public void clickOnTheSortByPriceLowHi() {
        System.out.println("Click on the sort by price lowhigh option.");
        this.sortLowHi.click();
    }

    public void clickOnTheSortByPriceHiLow() {
        System.out.println("Click on the sort by price highlow option.");
        this.sortHiLow.click();
    }


    public Product getFirstProductInList() {
        SelenideElement first = cards.first();
        return new Product(first);
    }

    public Product getLastProductInList() {
        SelenideElement last = cards.last();
        return new Product(last);
    }

    public void clickOnTheProductLink(int index) {
        if (index >= 0 && index < productLinks.size()) {
            System.out.println("Clicking on product link at index " + index);
            productLinks.get(index).click();
        } else {
            System.out.println("Invalid index: " + index);
        }
    }
}









