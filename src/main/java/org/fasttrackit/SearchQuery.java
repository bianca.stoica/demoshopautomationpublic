package org.fasttrackit;

public class SearchQuery {
    private String searchQuery;

    public SearchQuery(String searchQuery) {
        this.searchQuery = searchQuery;
    }

    public String getSearchQuery() {
        return searchQuery;
    }
    @Override
    public String toString() {
        return searchQuery;
    }
}

