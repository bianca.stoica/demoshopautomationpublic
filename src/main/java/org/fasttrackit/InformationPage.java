package org.fasttrackit;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;

public class InformationPage {
    private final SelenideElement firstNameField = $("#first-name");
    private final SelenideElement lastNameField = $("#last-name");
    private final SelenideElement addressField = $("#address");
    private final ElementsCollection radioButtons = (ElementsCollection) $$("div.form-check input[type='radio']");
    private final SelenideElement continueCheckoutButton = $(".btn-success");
    private final SelenideElement cancelButton = $(".btn-danger");
    private final SelenideElement errorMessage = $(".error");

    public void clickOnTheFirstNameField() {
        System.out.println("Click on the first name field.");
        firstNameField.click();
    }

    public void typeInFirstName(String firstname) {
        System.out.println("Type in " + firstname);
        firstNameField.setValue(firstname);
    }

    public void clickOnTheLastNameField() {
        System.out.println("Click on the last name field");
        lastNameField.click();
    }

    public void typeInLastName(String lastName) {
        System.out.println("Type in " + lastName);
        lastNameField.setValue(lastName);
    }

    public void clickOnTheAddressField() {
        System.out.println("Click on the address field.");
        addressField.click();
    }

    public void typeInAddress(String address) {
        System.out.println("Type in " + address);
        addressField.setValue(address);
    }

    public void clickOnTheChooChooDeliveryOption() {
        System.out.println("Click on the Choo Choo delivery option.");
        radioButtons.get(0).click();
    }

    public void clickOnTheCashOnDeliveryOption() {
        System.out.println("Click on the cash on delivery option.");
        radioButtons.get(1).click();
    }

    public void clickOnTheContinueCheckoutButton() {
        System.out.println("Click on the continue checkout button");
        continueCheckoutButton.click();
    }

    public void clickOnTheCancelButton() {
        System.out.println("Click on the 'Cancel' button");
        cancelButton.click();
    }

    public String getErrorMessage() {
        return errorMessage.text();
    }

    public String getFirstName() {
        return firstNameField.getValue();
    }

    public String getLastName() {
        return lastNameField.getValue();
    }

    public String getAddress() {
        return addressField.getValue();
    }


    public boolean isCreditCardOptionDisabled() {
        return radioButtons.get(2).is(Condition.disabled);
    }

    public boolean isPayPalOptionDisabled() {
        return radioButtons.get(3).is(Condition.disabled);
    }
    public boolean isErrorMessageDisplayed(){
        return errorMessage.isDisplayed();
    }
}