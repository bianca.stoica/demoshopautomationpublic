package org.fasttrackit;

public class YourInformation {
    private String firstName;
    private String lastName;
    private String address;

    public YourInformation(String firstName, String lastName, String address) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
    }

    @Override
    public String toString() {
        return firstName + "/" + lastName + "/" + address;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getAddress() {
        return address;
    }
}

