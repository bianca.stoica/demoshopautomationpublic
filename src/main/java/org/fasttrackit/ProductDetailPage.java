package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class ProductDetailPage {
    private final SelenideElement productTitle = $(".text-muted");
    private final SelenideElement productDescription = $("div.col:nth-child(2)");
    private final SelenideElement productImage = $(".product-image");
    private final SelenideElement addToCartButton = $(".btn-link .fa-cart-plus");
    private final SelenideElement addToWishListButton = $(".btn-link .fa-heart");

    public boolean getProductDescription() {
        return productDescription.isDisplayed();
    }

    public boolean getProductImage() {
        return productImage.isDisplayed();
    }

    public String getProductTitle() {
        return this.productTitle.text();
    }

    public void addProductToCart() {
        System.out.println("Click on the 'add to cart button'");
        addToCartButton.click();
    }

    public void addProductToWishlist() {
        System.out.println("Click on the 'add to wishlist button'");
        addToWishListButton.click();
    }
}



