package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class ModalDialog {
    private final SelenideElement username = $("#user-name");
    private final SelenideElement password = $("#password");
    private final SelenideElement loginButton = $(".modal-dialog .fa-sign-in-alt");
    private final SelenideElement errorElement = $(".error");
    private final SelenideElement closeButton = $(".close");
    private final SelenideElement modalTitle = $(".modal-title");


    public void typeInUsername(String user) {
        System.out.println("Click on the Username field.");
        username.click();
        System.out.println("Type in " + user);
        username.type(user);
    }

    public void typeInPassword(String pass) {
        System.out.println("Click on the Password field.");
        password.click();
        System.out.println("Type in " + pass);
        password.type(pass);
    }

    public void clickOnTheLoginButton() {
        System.out.println("Click on the Login button.");
        loginButton.click();
    }

    public boolean errorMessageIsDisplayed() {
        return errorElement.isDisplayed();
    }

    public String getErrorElement() {
        return this.errorElement.text();
    }

    public void clickOnTheCloseButton() {
        System.out.println("Click on the Close button.");
        closeButton.click();
    }

    public String getModalTitle() {
        return modalTitle.text();

    }

}
