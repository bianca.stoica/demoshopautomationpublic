package org.fasttrackit;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class Header {
    private final SelenideElement loginButton = $(".navbar .fa-sign-in-alt");
    private final SelenideElement greetingsElement = $(".navbar-text span span");
    private final SelenideElement wishListButton = $(".navbar .fa-heart");
    private final SelenideElement cartIcon = $(".fa-shopping-cart");
    private final SelenideElement shoppingCartBadge = $("a[href='#/cart'].shopping-cart-icon .fa-layers-counter.shopping_cart_badge");
    private final ElementsCollection shoppingCartBadges = $$("a[href='#/cart'].shopping-cart-icon .fa-layers-counter.shopping_cart_badge");
    private final SelenideElement homePageButton = $("[data-icon=shopping-bag]");
    private final SelenideElement logOutButton = $(".fa-sign-out-alt");
    private final SelenideElement wishlistBadge = $("a[href='#/wishlist'].shopping-cart-icon .fa-layers-counter.shopping_cart_badge");
    private final ElementsCollection wishlistBadges = $$("a[href='#/wishlist'].shopping-cart-icon .fa-layers-counter.shopping_cart_badge");

    public void clickOnTheLoginButton() {
        loginButton.click();
        System.out.println("Click on the Login Button.");
    }

    public String getGreetingsMessage() {
        return greetingsElement.text();
    }

    public void clickOnTheWishListIcon() {
        System.out.println("Click on the WishList button.");
        wishListButton.click();
    }

    public void clickOnTheShoppingBagIcon() {
        System.out.println("Click on the Shopping Bag Icon.");
        homePageButton.click();
    }

    public void clickOnTheCartIcon() {
        System.out.println("Click on the Cart Icon.");
        cartIcon.click();
    }

    public void clickOnTheLogOutButton() {
        System.out.println("Click on the Log Out Button.");
        logOutButton.click();
    }

    public String getShoppingCartBadgeValue() {
        return this.shoppingCartBadge.text();
    }

    public boolean isShoppingBadgeVisible() {
        return !this.shoppingCartBadges.isEmpty();
    }

    public String getWishlistBadgeValue() {
        return this.wishlistBadge.text();
    }

    public boolean isWishlistBadgeVisible() {
        return !this.wishlistBadges.isEmpty();
    }
}


