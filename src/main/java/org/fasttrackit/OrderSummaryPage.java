package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class OrderSummaryPage {
    private final SelenideElement completeYourOrderButton = $(".btn-success");
    private final SelenideElement cancelButton = $(".btn-danger");

    public void clickOnTheCompleteOrderButton() {
        System.out.println("Click on the 'Complete your order' button");
        completeYourOrderButton.click();
    }

    public void clickOnTheCancelButton() {
        System.out.println("Click on the 'Cancel' button");
        cancelButton.click();
    }
}
