package org.fasttrackit;


import io.qameta.allure.*;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import static org.testng.Assert.assertEquals;

public class PageNavigationTest {

    Page page = new Page();
    Header header = new Header();
    Footer footer = new Footer();
    CartPage cartPage = new CartPage();
    InformationPage informationPage = new InformationPage();
    OrderSummaryPage orderSummaryPage = new OrderSummaryPage();
    OrderCompletePage orderCompletePage = new OrderCompletePage();


    @BeforeClass
    public void setup() {
        page.openHomePage();
    }

    @AfterMethod
    public void clean_up() {
        footer.clickToReset();
        header.clickOnTheShoppingBagIcon();
    }

    @Test
    @Owner("Bianca Stoica")
    @Description("User can navigate to cart page.")
    @Severity(SeverityLevel.CRITICAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void user_can_navigate_to_cart_page() {
        header.clickOnTheCartIcon();
        assertEquals(page.getPageTitle(), "Your cart", "Expected to be on the Cart page");
    }

    @Test
    @Owner("Bianca Stoica")
    @Description("User can navigate to homepage from cart page.")
    @Severity(SeverityLevel.CRITICAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void user_can_navigate_to_homepage_from_cart_page() {
        header.clickOnTheCartIcon();
        header.clickOnTheShoppingBagIcon();
        assertEquals(page.getPageTitle(), "Products", "Expected to be on Products page");
    }

    @Test
    @Owner("Bianca Stoica")
    @Description("User can navigate to 'Wishlist' page.")
    @Severity(SeverityLevel.CRITICAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void user_can_navigate_to_Wishlist_Page() {
        header.clickOnTheWishListIcon();
        assertEquals(page.getPageTitle(), "Wishlist", "Expected to be on the Wishlist page.");
    }

    @Test
    @Owner("Bianca Stoica")
    @Description("User can navigate to homepage from 'Wishlist' page.")
    @Severity(SeverityLevel.CRITICAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void user_can_navigate_to_Home_Page_from_Wishlist_Page() {
        header.clickOnTheWishListIcon();
        header.clickOnTheShoppingBagIcon();
        assertEquals(page.getPageTitle(), "Products", "Expected to be on the Products page.");
    }

    @Test
    @Owner("Bianca Stoica")
    @Description("User can navigate to information page from Cart page.")
    @Severity(SeverityLevel.CRITICAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void user_can_navigate_from_Cart_Page_to_Information_Page() {
        Product product = new Product("1");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheCheckoutButton();
        assertEquals(page.getPageTitle(), "Your information", "Expected to be on the 'Your information' page.");
    }


    @Test
    @Owner("Bianca Stoica")
    @Description("User can navigate to Order Summary page to Order Complete page.")
    @Severity(SeverityLevel.CRITICAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void user_can_navigate_from_Order_Summary_page_to_Order_complete_Page() {
        Product product = new Product("1");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheCheckoutButton();
        informationPage.clickOnTheFirstNameField();
        informationPage.typeInFirstName("Jane");
        informationPage.clickOnTheLastNameField();
        informationPage.typeInLastName("Doe");
        informationPage.clickOnTheAddressField();
        informationPage.typeInAddress("New York Fifth Avenue");
        informationPage.clickOnTheChooChooDeliveryOption();
        informationPage.clickOnTheCashOnDeliveryOption();
        informationPage.clickOnTheContinueCheckoutButton();
        orderSummaryPage.clickOnTheCompleteOrderButton();
        assertEquals(page.getPageTitle(), "Order complete", "Expected to be on the 'Order Complete' page");
    }

    @Test
    @Owner("Bianca Stoica")
    @Description("User can navigate from Order Summary to previous Your Information page.")
    @Severity(SeverityLevel.CRITICAL)
    @Issue("BUG-9")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void user_can_navigate_from_Order_Summary_to_previous_Your_Information_Page() {
        Product product = new Product("1");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheCheckoutButton();
        informationPage.clickOnTheFirstNameField();
        informationPage.typeInFirstName("Jane");
        informationPage.clickOnTheLastNameField();
        informationPage.typeInLastName("Doe");
        informationPage.clickOnTheAddressField();
        informationPage.typeInAddress("New York Fifth Avenue");
        informationPage.clickOnTheChooChooDeliveryOption();
        informationPage.clickOnTheCashOnDeliveryOption();
        informationPage.clickOnTheContinueCheckoutButton();
        orderSummaryPage.clickOnTheCancelButton();
        assertEquals(page.getPageTitle(), "Your information", "Expected to be on the 'Your information' page");
    }

    @Test
    @Owner("Bianca Stoica")
    @Description("User can navigate from Your Information Page to previous Your Cart page.")
    @Severity(SeverityLevel.CRITICAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void user_can_navigate_from_Your_Information_Page_to_previous_Your_Cart_Page() {
        page.openHomePage();
        Product product = new Product("1");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheCheckoutButton();
        informationPage.clickOnTheCancelButton();
        assertEquals(page.getPageTitle(), "Your cart", "Expected to be on the 'Your cart' page");
    }

    @Test
    @Owner("Bianca Stoica")
    @Description("User can navigate from Order Complete page to homepage.")
    @Severity(SeverityLevel.CRITICAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void user_can_navigate_from_Order_Complete_Page_to_Home_Page() {
        Product product = new Product("1");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheCheckoutButton();
        informationPage.clickOnTheFirstNameField();
        informationPage.typeInFirstName("Jane");
        informationPage.clickOnTheLastNameField();
        informationPage.typeInLastName("Doe");
        informationPage.clickOnTheAddressField();
        informationPage.typeInAddress("New York Fifth Avenue");
        informationPage.clickOnTheChooChooDeliveryOption();
        informationPage.clickOnTheCashOnDeliveryOption();
        informationPage.clickOnTheContinueCheckoutButton();
        orderSummaryPage.clickOnTheCompleteOrderButton();
        orderCompletePage.clickOnTheContinueShoppingButton();
        assertEquals(page.getPageTitle(), "Products", "Expected to be on the 'Products' page");

    }

    @Test
    @Owner("Bianca Stoica")
    @Description("User can navigate from Order Complete page to homepage.")
    @Severity(SeverityLevel.CRITICAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void user_can_navigate_from_homepage_to_product_detail_page() {
        ProductCards productCards = new ProductCards();
        productCards.clickOnTheProductLink(2);
        assertEquals(page.getPageTitle(), "Awesome Soft Shirt", "Expected to be on the 'Awesome Soft Shirt' page");
    }

    @Test
    @Owner("Bianca Stoica")
    @Description("User can navigate to homepage from product detail page.")
    @Severity(SeverityLevel.CRITICAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void user_can_navigate_to_homepage_from_product_detail_page() {
        ProductCards productCards = new ProductCards();
        productCards.clickOnTheProductLink(2);
        header.clickOnTheShoppingBagIcon();
        assertEquals(page.getPageTitle(), "Products", "Expected to be on the 'Products' page");

    }

    @Test
    @Owner("Bianca Stoica")
    @Description("User can navigate to homepage from your information page.")
    @Severity(SeverityLevel.CRITICAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void user_can_navigate_to_homepage_from_your_information_page() {
        Product product = new Product("1");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheCheckoutButton();
        header.clickOnTheShoppingBagIcon();
        assertEquals(page.getPageTitle(), "Products", "Expected to be on the 'Products' page");
    }

    @Test
    @Owner("Bianca Stoica")
    @Description("User can navigate to homepage from Order Summary page.")
    @Severity(SeverityLevel.CRITICAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void user_can_navigate_to_homepage_from_Order_Summary_Page() {
        Product product = new Product("1");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheCheckoutButton();
        informationPage.clickOnTheFirstNameField();
        informationPage.typeInFirstName("Jane");
        informationPage.clickOnTheLastNameField();
        informationPage.typeInLastName("Doe");
        informationPage.clickOnTheAddressField();
        informationPage.typeInAddress("New York Fifth Avenue");
        informationPage.clickOnTheChooChooDeliveryOption();
        informationPage.clickOnTheCashOnDeliveryOption();
        informationPage.clickOnTheContinueCheckoutButton();
        header.clickOnTheShoppingBagIcon();
        assertEquals(page.getPageTitle(), "Products", "Expected to be on the 'Products' page");
    }
}

