package org.fasttrackit.dataprovider;

import org.fasttrackit.InvalidSearchQuery;
import org.fasttrackit.SearchQuery;
import org.fasttrackit.ValidSearchQuery;
import org.testng.annotations.DataProvider;

import static org.fasttrackit.dataprovider.ProductsDataProvider.*;

public class UserAndSearchBarDataProvider {
    static ValidSearchQuery aws = new ValidSearchQuery("Awesome");
    static ValidSearchQuery gnt = new ValidSearchQuery("Granite");
    static ValidSearchQuery mus = new ValidSearchQuery("Mouse");
    static InvalidSearchQuery numb = new InvalidSearchQuery("123");
    static InvalidSearchQuery spc = new InvalidSearchQuery("!@#");
    static InvalidSearchQuery nep = new InvalidSearchQuery("NonexistentProduct");
    static ValidSearchQuery glv = new ValidSearchQuery("Gloves");
    static ValidSearchQuery bcn = new ValidSearchQuery("Bacon");
    static ValidSearchQuery awe = new ValidSearchQuery("Awe");
    static ValidSearchQuery den = new ValidSearchQuery("den");
    static ValidSearchQuery ncr = new ValidSearchQuery("ncr");
    static InvalidSearchQuery dlr = new InvalidSearchQuery("$");
    static InvalidSearchQuery prc = new InvalidSearchQuery("19.99");
    static ValidSearchQuery hat = new ValidSearchQuery("HAT");
    static InvalidSearchQuery pra = new InvalidSearchQuery("Practical  Awesome");
    static ValidSearchQuery g = new ValidSearchQuery("g");
    static ValidSearchQuery mtl = new ValidSearchQuery("MeTaL");


    @DataProvider(name = "validUserAndValidSearchQuery")
    public Object[][] getvalidUserAndValidSearchQuery() {
        return new Object[][]{
                {AuthenticationDataProvider.dino, aws},
                {AuthenticationDataProvider.dino, gnt},
                {AuthenticationDataProvider.dino, mus},
                {AuthenticationDataProvider.dino, glv},
                {AuthenticationDataProvider.dino, bcn},
                {AuthenticationDataProvider.dino, awe},
                {AuthenticationDataProvider.dino, den},
                {AuthenticationDataProvider.dino, ncr},
                {AuthenticationDataProvider.dino, hat},
                {AuthenticationDataProvider.dino, g},
                {AuthenticationDataProvider.dino, mtl},
                {AuthenticationDataProvider.turtle, aws},
                {AuthenticationDataProvider.turtle, gnt},
                {AuthenticationDataProvider.turtle, mus},
                {AuthenticationDataProvider.turtle, glv},
                {AuthenticationDataProvider.turtle, bcn},
                {AuthenticationDataProvider.turtle, awe},
                {AuthenticationDataProvider.turtle, den},
                {AuthenticationDataProvider.turtle, ncr},
                {AuthenticationDataProvider.turtle, hat},
                {AuthenticationDataProvider.turtle, g},
                {AuthenticationDataProvider.turtle, mtl},
                {AuthenticationDataProvider.beetle, aws},
                {AuthenticationDataProvider.beetle, gnt},
                {AuthenticationDataProvider.beetle, mus},
                {AuthenticationDataProvider.beetle, glv},
                {AuthenticationDataProvider.beetle, bcn},
                {AuthenticationDataProvider.beetle, awe},
                {AuthenticationDataProvider.beetle, den},
                {AuthenticationDataProvider.beetle, ncr},
                {AuthenticationDataProvider.beetle, hat},
                {AuthenticationDataProvider.beetle, g},
                {AuthenticationDataProvider.beetle, mtl},
        };

    }

    @DataProvider(name = "validUserAndInvalidSearchQuery")
    public Object[][] getValidUserProductAndAndValidSearchQuery() {
        return new Object[][]{
                {AuthenticationDataProvider.dino, numb},
                {AuthenticationDataProvider.dino, spc,},
                {AuthenticationDataProvider.dino, nep},
                {AuthenticationDataProvider.dino, dlr},
                {AuthenticationDataProvider.dino, prc},
                {AuthenticationDataProvider.dino, pra},
                {AuthenticationDataProvider.turtle, numb},
                {AuthenticationDataProvider.turtle, spc,},
                {AuthenticationDataProvider.turtle, nep},
                {AuthenticationDataProvider.turtle, dlr},
                {AuthenticationDataProvider.turtle, prc},
                {AuthenticationDataProvider.turtle, pra},
                {AuthenticationDataProvider.beetle, numb},
                {AuthenticationDataProvider.beetle, spc,},
                {AuthenticationDataProvider.beetle, nep},
                {AuthenticationDataProvider.beetle, dlr},
                {AuthenticationDataProvider.beetle, prc},
                {AuthenticationDataProvider.beetle, pra},
        };
    }
}

