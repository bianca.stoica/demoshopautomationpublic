package org.fasttrackit.dataprovider;

import org.fasttrackit.ProductData;
import org.testng.annotations.DataProvider;

import java.util.Arrays;
import java.util.List;

import static org.fasttrackit.dataprovider.ProductsDataProvider.*;
import static org.fasttrackit.dataprovider.ProductsDataProvider.rfm;

public class UsersAndWishlistDataProvider {
    @DataProvider(name = "userAndWishlistProduct")
    public Object[][] getUserAndProduct() {
        return new Object[][]{
                {AuthenticationDataProvider.dino, agc},
                {AuthenticationDataProvider.dino, ich},
                {AuthenticationDataProvider.dino, amc},
                {AuthenticationDataProvider.dino, ass},
                {AuthenticationDataProvider.dino, gsp},
                {AuthenticationDataProvider.dino, lsg},
                {AuthenticationDataProvider.dino, pmm},
                {AuthenticationDataProvider.dino, pwb},
                {AuthenticationDataProvider.dino, pwbs},
                {AuthenticationDataProvider.dino, rfm},
                {AuthenticationDataProvider.turtle, agc},
                {AuthenticationDataProvider.turtle, ich},
                {AuthenticationDataProvider.turtle, amc},
                {AuthenticationDataProvider.turtle, ass},
                {AuthenticationDataProvider.turtle, gsp},
                {AuthenticationDataProvider.turtle, lsg},
                {AuthenticationDataProvider.turtle, pmm},
                {AuthenticationDataProvider.turtle, pwb},
                {AuthenticationDataProvider.turtle, pwbs},
                {AuthenticationDataProvider.turtle, rfm},
                {AuthenticationDataProvider.beetle, agc},
                {AuthenticationDataProvider.beetle, ich},
                {AuthenticationDataProvider.beetle, amc},
                {AuthenticationDataProvider.beetle, ass},
                {AuthenticationDataProvider.beetle, gsp},
                {AuthenticationDataProvider.beetle, lsg},
                {AuthenticationDataProvider.beetle, pmm},
                {AuthenticationDataProvider.beetle, pwb},
                {AuthenticationDataProvider.beetle, pwbs},
                {AuthenticationDataProvider.beetle, rfm},
        };
    }

    @DataProvider(name = "userAndWishlistProducts")
    public Object[][] getUsersAndProducts() {
        List<ProductData> productsBeetle = Arrays.asList(agc, ich, amc, ass, gsp, lsg, pmm, pwbs, pwb, rfm);
        List<ProductData> productsDino = Arrays.asList(agc, ich, amc, ass, gsp, lsg, pmm, pwbs, pwb, rfm);
        List<ProductData> productsTurtle = Arrays.asList(agc, ich, amc, ass, gsp, lsg, pmm, pwbs, pwb, rfm);
        return new Object[][]{
                {AuthenticationDataProvider.beetle, productsBeetle},
                {AuthenticationDataProvider.turtle, productsDino},
                {AuthenticationDataProvider.dino, productsTurtle},
        };
    }
}
