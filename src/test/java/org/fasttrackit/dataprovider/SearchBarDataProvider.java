package org.fasttrackit.dataprovider;

import org.fasttrackit.InvalidSearchQuery;
import org.fasttrackit.SearchQuery;
import org.fasttrackit.ValidSearchQuery;
import org.testng.annotations.DataProvider;


public class SearchBarDataProvider {
    static ValidSearchQuery aws = new ValidSearchQuery("Awesome");
    static ValidSearchQuery gnt = new ValidSearchQuery("Granite");
    static ValidSearchQuery mus = new ValidSearchQuery("Mouse");
    static InvalidSearchQuery numb = new InvalidSearchQuery("123");
    static InvalidSearchQuery spc = new InvalidSearchQuery("!@#");
    static InvalidSearchQuery nep = new InvalidSearchQuery("NonexistentProduct");
    static ValidSearchQuery glv = new ValidSearchQuery("Gloves");
    static ValidSearchQuery bcn = new ValidSearchQuery("Bacon");
    static ValidSearchQuery awe = new ValidSearchQuery("Awe");
    static ValidSearchQuery den = new ValidSearchQuery("den");
    static ValidSearchQuery ncr = new ValidSearchQuery("ncr");
    static InvalidSearchQuery dlr = new InvalidSearchQuery("$");
    static InvalidSearchQuery prc = new InvalidSearchQuery("19.99");
    static ValidSearchQuery hat = new ValidSearchQuery("HAT");
    static InvalidSearchQuery pra = new InvalidSearchQuery("Practical  Awesome");
    static ValidSearchQuery g = new ValidSearchQuery("g");
    static ValidSearchQuery mtl = new ValidSearchQuery("MeTaL");

    @DataProvider(name = "searchQueries")
    public Object[][] getSearchQueries() {
        return new Object[][]{
                {aws},
                {gnt},
                {mus},
                {numb},
                {spc},
                {nep},
                {glv},
                {bcn},
                {awe},
                {den},
                {ncr},
                {dlr},
                {prc},
                {hat},
                {pra},
                {g},
                {mtl},
        };

    }
}



