package org.fasttrackit.dataprovider;

import org.fasttrackit.ProductData;
import org.testng.annotations.DataProvider;

public class WishlistDataProvider {
    static ProductData agc = new ProductData("1", "Awesome Granite Chips", "15.99");
    static ProductData ich = new ProductData("2", "Incredible Concrete Hat", "7.99");
    static ProductData amc = new ProductData("3", "Awesome Metal Chair", "15.99");
    static ProductData ass = new ProductData("5", "Awesome Soft Shirt", "29.99");
    static ProductData gsp = new ProductData("9", "Gorgeous Soft Pizza", "19.99");
    static ProductData lsg = new ProductData("8", "Licensed Steel Gloves", "14.99");
    static ProductData pmm = new ProductData("7", "Practical Metal Mouse", "9.99");
    static ProductData pwb = new ProductData("4", "Practical Wooden Bacon", "29.99");
    static ProductData pwbs = new ProductData("6", "Practical Wooden Bacon", "1.99");
    static ProductData rfm = new ProductData("0", "Refined Frozen Mouse", "9.99");


    @DataProvider(name = "wishlist product")
    public Object[][] getCredentials() {

        return new Object[][]{
                {agc},
                {ich},
                {amc},
                {ass},
                {gsp},
                {lsg},
                {pmm},
                {pwb},
                {pwbs},
                {rfm},
        };
    }
}
