package org.fasttrackit.dataprovider;

import org.fasttrackit.InvalidInformation;
import org.fasttrackit.YourInformation;
import org.testng.annotations.DataProvider;

public class YourInformationDataProvider {

    static YourInformation validInfo1 = new YourInformation("Jane", "Doe", "123 Main Street");
    static YourInformation validInfo2 = new YourInformation("John", "Smith", "456 Oak Ave");
    static InvalidInformation noUsername = new InvalidInformation(" ", "Doe", "123 Main Street");
    static InvalidInformation noPassword = new InvalidInformation("John", " ", "123 Main Street");
    static InvalidInformation noAddress = new InvalidInformation("John", "Doe", " ");
    static InvalidInformation invalidUsername = new InvalidInformation("@#$", "Smith", "22 Wall Street");
    static InvalidInformation invalidLastname = new InvalidInformation("Peter", ".;;''", "13 Wall Street");
    static InvalidInformation invalidAddress = new InvalidInformation("Peter", "", "13 Wall Street");
    static InvalidInformation noULP = new InvalidInformation("", "", "13 Wall Street");

    @DataProvider(name = "validInfoProvider")
    public static Object[][] provideUserData() {
        return new Object[][]{
                {validInfo1},
                {validInfo2},
        };
    }

    @DataProvider(name = "invalidInfoProvider")
    public static Object[][] provideInvalidUserData() {
        return new Object[][]{
                {noUsername},
                {noPassword},
                {noAddress},
                {noUsername},
                {invalidUsername},
                {invalidLastname},
                {invalidAddress},
                {noULP},
        };
    }
}