package org.fasttrackit.dataprovider;

import org.fasttrackit.InvalidInformation;
import org.fasttrackit.ValidInformation;
import org.testng.annotations.DataProvider;

public class UsersAndInformationPageDataProvider {
    static ValidInformation validInfo1 = new ValidInformation("Jane", "Doe", "123 Main Street");
    static ValidInformation validInfo2 = new ValidInformation("John", "Smith", "456 Oak Ave");
    static InvalidInformation noFirstname = new InvalidInformation(" ", "Doe", "123 Main Street");
    static InvalidInformation noPassword = new InvalidInformation("John", " ", "123 Main Street");
    static InvalidInformation noAddress = new InvalidInformation("John", "Doe", " ");
    static InvalidInformation invalidFirstname = new InvalidInformation("@#$", "Smith", "22 Wall Street");
    static InvalidInformation invalidLastname = new InvalidInformation("Peter", ".;;''", "13 Wall Street");
    static InvalidInformation invalidAddress = new InvalidInformation("Peter", "", "13 Wall Street");
    static InvalidInformation noULP = new InvalidInformation("", "", "13 Wall Street");

    @DataProvider(name = "validUserAndInfo")
    public Object[][] getUserAndInfo() {
        return new Object[][]{
                {AuthenticationDataProvider.dino, validInfo1},
                {AuthenticationDataProvider.dino, validInfo2},
                {AuthenticationDataProvider.beetle, validInfo1},
                {AuthenticationDataProvider.beetle, validInfo2},
                {AuthenticationDataProvider.turtle, validInfo1},
                {AuthenticationDataProvider.turtle, validInfo2},
        };
    }

    @DataProvider(name = "validUserProductAndInfo")
    public Object[][] getUserProductAndInfo() {
        return new Object[][]{
                {AuthenticationDataProvider.dino, ProductsDataProvider.lsg, validInfo1},
                {AuthenticationDataProvider.dino, ProductsDataProvider.agc, validInfo2},
                {AuthenticationDataProvider.beetle, ProductsDataProvider.agc, validInfo1},
                {AuthenticationDataProvider.beetle, ProductsDataProvider.lsg, validInfo2},
                {AuthenticationDataProvider.turtle, ProductsDataProvider.lsg, validInfo1},
                {AuthenticationDataProvider.turtle, ProductsDataProvider.ich, validInfo2},

        };
    }

    @DataProvider(name = "validUserProductAndInvalidInfo")
    public Object[][] getUserProductAndInvalidInfo() {
        return new Object[][]{
                {AuthenticationDataProvider.dino, ProductsDataProvider.lsg, noFirstname},
                {AuthenticationDataProvider.dino, ProductsDataProvider.agc, noPassword},
                {AuthenticationDataProvider.beetle, ProductsDataProvider.agc, noULP},
                {AuthenticationDataProvider.beetle, ProductsDataProvider.lsg, noAddress},
                {AuthenticationDataProvider.turtle, ProductsDataProvider.lsg, invalidAddress},
                {AuthenticationDataProvider.turtle, ProductsDataProvider.ich, invalidFirstname},
                {AuthenticationDataProvider.dino, ProductsDataProvider.agc, invalidLastname}
        };
    }
}
