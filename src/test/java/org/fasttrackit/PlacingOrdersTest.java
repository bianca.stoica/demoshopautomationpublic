package org.fasttrackit;

import io.qameta.allure.*;
import org.fasttrackit.dataprovider.ProductsDataProvider;
import org.fasttrackit.dataprovider.UsersAndProductsDataProvider;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import java.util.List;
import static org.testng.Assert.*;

public class PlacingOrdersTest {
    Page page = new Page();
    Header header = new Header();
    Footer footer = new Footer();
    CartPage cartPage = new CartPage();
    InformationPage informationPage = new InformationPage();
    OrderSummaryPage orderSummaryPage = new OrderSummaryPage();
    OrderCompletePage orderCompletePage = new OrderCompletePage();
    ModalDialog modal = new ModalDialog();

    @BeforeClass
    public void setup() {
        page.openHomePage();
    }

    @AfterMethod
    public void cleanup() {
        footer.clickToReset();
        header.clickOnTheShoppingBagIcon();
    }

    @Test(dataProviderClass = ProductsDataProvider.class, dataProvider = "products")
    @Owner("Bianca Stoica")
    @Description("User can place a one product order.")
    @Severity(SeverityLevel.CRITICAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void user_can_place_a_one_product_order(ProductData productData) {
        productData.getProduct().clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheCheckoutButton();
        informationPage.clickOnTheFirstNameField();
        informationPage.typeInFirstName("Jane");
        informationPage.clickOnTheLastNameField();
        informationPage.typeInLastName("Doe");
        informationPage.clickOnTheAddressField();
        informationPage.typeInAddress("New York Fifth Avenue");
        informationPage.clickOnTheChooChooDeliveryOption();
        informationPage.clickOnTheCashOnDeliveryOption();
        informationPage.clickOnTheContinueCheckoutButton();
        orderSummaryPage.clickOnTheCompleteOrderButton();
        assertTrue(orderCompletePage.completeOrderMessageIsDisplayed());
    }

    @Test(dataProviderClass = UsersAndProductsDataProvider.class, dataProvider = "userAndProduct")
    @Owner("Bianca Stoica")
    @Description("Valid users can place a one product order.")
    @Severity(SeverityLevel.CRITICAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void valid_users_can_place_a_one_product_order(ValidAccount account, ProductData productData) {
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        productData.getProduct().clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheCheckoutButton();
        informationPage.clickOnTheFirstNameField();
        informationPage.typeInFirstName("Jane");
        informationPage.clickOnTheLastNameField();
        informationPage.typeInLastName("Doe");
        informationPage.clickOnTheAddressField();
        informationPage.typeInAddress("New York Fifth Avenue");
        informationPage.clickOnTheChooChooDeliveryOption();
        informationPage.clickOnTheCashOnDeliveryOption();
        informationPage.clickOnTheContinueCheckoutButton();
        orderSummaryPage.clickOnTheCompleteOrderButton();
        assertTrue(orderCompletePage.completeOrderMessageIsDisplayed());
    }

    @Test(dataProviderClass = UsersAndProductsDataProvider.class, dataProvider = "userAndProducts")
    @Owner("Bianca Stoica")
    @Description("Valid users can can place a multiple products order.")
    @Severity(SeverityLevel.CRITICAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void valid_users_can_place_a_multiple_products_order(ValidAccount account, List<ProductData> productDataList) {
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        for (ProductData currentProductData : productDataList) {
            Product currentProduct = currentProductData.getProduct();
            currentProduct.clickOnTheProductCartIcon();
            System.out.println("Added product to cart: " + currentProductData.getName());
        }
        header.clickOnTheCartIcon();
        cartPage.clickOnTheCheckoutButton();
        informationPage.clickOnTheFirstNameField();
        informationPage.typeInFirstName("Jane");
        informationPage.clickOnTheLastNameField();
        informationPage.typeInLastName("Doe");
        informationPage.clickOnTheAddressField();
        informationPage.typeInAddress("New York Fifth Avenue");
        informationPage.clickOnTheChooChooDeliveryOption();
        informationPage.clickOnTheCashOnDeliveryOption();
        informationPage.clickOnTheContinueCheckoutButton();
        orderSummaryPage.clickOnTheCompleteOrderButton();
        assertTrue(orderCompletePage.completeOrderMessageIsDisplayed());
    }

    @Test(dataProviderClass = UsersAndProductsDataProvider.class, dataProvider = "userAndProduct")
    @Owner("Bianca Stoica")
    @Description("Valid users can not continue checkout process without completing your information section.")
    @Severity(SeverityLevel.CRITICAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void valid_users_can_not_continue_checkout_process_without_completing_your_information_section(ValidAccount account, ProductData productData) {
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        productData.getProduct().clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheCheckoutButton();
        informationPage.clickOnTheContinueCheckoutButton();
        assertEquals(informationPage.getErrorMessage(), "First Name is required", "Expected to get error message 'First Name is required'");
    }

    @Test
    @Owner("Bianca Stoica")
    @Description("User can not continue checkout process without completing your information section.")
    @Severity(SeverityLevel.CRITICAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void user_can_not_continue_checkout_process_without_completing_your_information_section() {
        Product product = new Product("2");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheCheckoutButton();
        informationPage.clickOnTheContinueCheckoutButton();
        assertEquals(informationPage.getErrorMessage(), "First Name is required", "Expected to get error message 'First Name is required'");
    }

    @Test(dataProviderClass = UsersAndProductsDataProvider.class, dataProvider = "userAndProducts")
    @Owner("Bianca Stoica")
    @Description("Valid users can not continue checkout process without completing first name field.")
    @Severity(SeverityLevel.CRITICAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void valid_users_can_not_continue_checkout_process_without_completing_first_name_field(ValidAccount account, List<ProductData> productDataList) {
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        for (ProductData currentProductData : productDataList) {
            Product currentProduct = currentProductData.getProduct();
            currentProduct.clickOnTheProductCartIcon();
            System.out.println("Added product to cart: " + currentProductData.getName());
        }
        header.clickOnTheCartIcon();
        cartPage.clickOnTheCheckoutButton();
        informationPage.clickOnTheLastNameField();
        informationPage.typeInLastName("Doe");
        informationPage.clickOnTheAddressField();
        informationPage.typeInAddress("New York Fifth Avenue");
        informationPage.clickOnTheChooChooDeliveryOption();
        informationPage.clickOnTheCashOnDeliveryOption();
        informationPage.clickOnTheContinueCheckoutButton();
        assertEquals(informationPage.getErrorMessage(), "First Name is required", "Expected to get error message 'First Name is required'");
    }

    @Test
    @Owner("Bianca Stoica")
    @Description("User can not continue checkout process without completing first name field.")
    @Severity(SeverityLevel.CRITICAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void user_can_not_continue_checkout_process_without_completing_first_name_field() {
        Product product = new Product("5");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheCheckoutButton();
        informationPage.clickOnTheLastNameField();
        informationPage.typeInLastName("Doe");
        informationPage.clickOnTheAddressField();
        informationPage.typeInAddress("New York Fifth Avenue");
        informationPage.clickOnTheChooChooDeliveryOption();
        informationPage.clickOnTheCashOnDeliveryOption();
        informationPage.clickOnTheContinueCheckoutButton();
        assertEquals(informationPage.getErrorMessage(), "First Name is required", "Expected to get error message 'First Name is required'");
    }

    @Test
    @Owner("Bianca Stoica")
    @Description("User can not continue checkout process without completing last name field.")
    @Severity(SeverityLevel.CRITICAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void user_can_not_continue_checkout_process_without_completing_last_name_field() {
        Product product = new Product("5");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheCheckoutButton();
        informationPage.clickOnTheFirstNameField();
        informationPage.typeInFirstName("Jane");
        informationPage.clickOnTheAddressField();
        informationPage.typeInAddress("New York Fifth Avenue");
        informationPage.clickOnTheChooChooDeliveryOption();
        informationPage.clickOnTheCashOnDeliveryOption();
        informationPage.clickOnTheContinueCheckoutButton();
        assertEquals(informationPage.getErrorMessage(), "Last Name is required", "Expected to get error message 'Last Name is required'");
    }

    @Test(dataProviderClass = UsersAndProductsDataProvider.class, dataProvider = "userAndProducts")
    @Owner("Bianca Stoica")
    @Description("Valid users can not continue checkout process without completing last name field.")
    @Severity(SeverityLevel.CRITICAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void valid_users_can_not_continue_checkout_process_without_completing_last_name_field(ValidAccount account, List<ProductData> productDataList) {
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        for (ProductData currentProductData : productDataList) {
            Product currentProduct = currentProductData.getProduct();
            currentProduct.clickOnTheProductCartIcon();
            System.out.println("Added product to cart: " + currentProductData.getName());
        }
        header.clickOnTheCartIcon();
        cartPage.clickOnTheCheckoutButton();
        informationPage.clickOnTheFirstNameField();
        informationPage.typeInFirstName("Jane");
        informationPage.clickOnTheAddressField();
        informationPage.typeInAddress("New York Fifth Avenue");
        informationPage.clickOnTheChooChooDeliveryOption();
        informationPage.clickOnTheCashOnDeliveryOption();
        informationPage.clickOnTheContinueCheckoutButton();
        assertEquals(informationPage.getErrorMessage(), "Last Name is required", "Expected to get error message 'Last Name is required'");
    }

    @Test
    @Owner("Bianca Stoica")
    @Description("User can not continue checkout process without completing first name field.")
    @Severity(SeverityLevel.CRITICAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void user_can_not_continue_checkout_process_without_completing_address_field() {
        Product product = new Product("5");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheCheckoutButton();
        informationPage.clickOnTheFirstNameField();
        informationPage.typeInFirstName("Jane");
        informationPage.clickOnTheLastNameField();
        informationPage.typeInLastName("Doe");
        informationPage.clickOnTheChooChooDeliveryOption();
        informationPage.clickOnTheCashOnDeliveryOption();
        informationPage.clickOnTheContinueCheckoutButton();
        assertEquals(informationPage.getErrorMessage(), "Address is required", "Expected to get error message 'Address is required'");
    }

    @Test(dataProviderClass = UsersAndProductsDataProvider.class, dataProvider = "userAndProducts")
    @Owner("Bianca Stoica")
    @Description("Valid users can not continue checkout process without completing address field.")
    @Severity(SeverityLevel.CRITICAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void valid_users_can_not_continue_checkout_process_without_completing_address_field(ValidAccount account, List<ProductData> productDataList) {
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        for (ProductData currentProductData : productDataList) {
            Product currentProduct = currentProductData.getProduct();
            currentProduct.clickOnTheProductCartIcon();
            System.out.println("Added product to cart: " + currentProductData.getName());
        }
        header.clickOnTheCartIcon();
        cartPage.clickOnTheCheckoutButton();
        informationPage.clickOnTheFirstNameField();
        informationPage.typeInFirstName("Jane");
        informationPage.clickOnTheLastNameField();
        informationPage.typeInLastName("Doe");
        informationPage.clickOnTheChooChooDeliveryOption();
        informationPage.clickOnTheCashOnDeliveryOption();
        informationPage.clickOnTheContinueCheckoutButton();
        assertEquals(informationPage.getErrorMessage(), "Address is required", "Expected to get error message 'Address is required'");
    }

}
