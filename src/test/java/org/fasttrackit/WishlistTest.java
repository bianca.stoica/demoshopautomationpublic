package org.fasttrackit;

import io.qameta.allure.*;
import org.fasttrackit.dataprovider.UsersAndWishlistDataProvider;
import org.fasttrackit.dataprovider.WishlistDataProvider;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.List;

import static com.codeborne.selenide.Selenide.sleep;
import static org.testng.Assert.*;

public class WishlistTest {

    Page page = new Page();
    Header header = new Header();
    Footer footer = new Footer();
    WishlistPage wishlistPage = new WishlistPage();
    ProductDetailPageTest productDetailPage = new ProductDetailPageTest();
    ModalDialog modal = new ModalDialog();

    @BeforeClass
    public void setup() {
        page.openHomePage();
    }

    @AfterMethod
    public void clean_up() {
        header.clickOnTheShoppingBagIcon();
        footer.clickToReset();
    }

    @Test(dataProviderClass = WishlistDataProvider.class, dataProvider = "wishlist product")
    @Owner("Bianca Stoica")
    @Description("User can add a product in wishlist.")
    @Severity(SeverityLevel.NORMAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void user_can_add_a_product_in_wishlist(ProductData productData) {
        productData.getProduct().clickOnTheWishListButton();
        assertTrue(header.isWishlistBadgeVisible());
        String wishlistBadgeValue = header.getWishlistBadgeValue();
        assertEquals(wishlistBadgeValue, "1", "After adding 1 product in wishlist, badge shows 1");

    }

    @Test(dataProviderClass = UsersAndWishlistDataProvider.class, dataProvider = "userAndWishlistProduct")
    @Owner("Bianca Stoica")
    @Description("User can add a product in wishlist.")
    @Severity(SeverityLevel.NORMAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void valid_users_can_add_a_product_in_wishlist(ValidAccount account, ProductData productData) {
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        productData.getProduct().clickOnTheWishListButton();
        assertTrue(header.isWishlistBadgeVisible());
        String wishlistBadgeValue = header.getWishlistBadgeValue();
        assertEquals(wishlistBadgeValue, "1", "After adding 1 product in wishlist, badge shows 1");

    }

    @Test(dataProviderClass = UsersAndWishlistDataProvider.class, dataProvider = "userAndWishlistProducts")
    @Owner("Bianca Stoica")
    @Description("Valid users can add multiple products in wishlist.")
    @Severity(SeverityLevel.NORMAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void valid_users_can_add_multiple_products_in_wishlist(ValidAccount account, List<ProductData> productDataList) {
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();

        for (ProductData currentProductData : productDataList) {
            Product currentProduct = currentProductData.getProduct();
            currentProduct.clickOnTheWishListButton();
            System.out.println("Added product to wishlist: " + currentProductData.getName());
        }

        header.getWishlistBadgeValue();
        assertFalse(wishlistPage.isWishlistPageEmpty(), "Wishlist page is empty.");

    }

    @Test(dataProviderClass = WishlistDataProvider.class, dataProvider = "wishlist product")
    @Owner("Bianca Stoica")
    @Description("Verify add to wishlist button changes appearance on click.")
    @Severity(SeverityLevel.NORMAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void verify_add_to_wishlist_button_changes_appearance_on_click(ProductData productData) {
        productData.getProduct().clickOnTheWishListButton();
        assertTrue(productData.getProduct().isRemoveWishlistButtonDisplayed(), "Wishlist button appearance should change when clicked.");
    }

    @Test(dataProviderClass = WishlistDataProvider.class, dataProvider = "wishlist product")
    @Owner("Bianca Stoica")
    @Description("Remove from wishlist button changes back to normal state after click.")
    @Severity(SeverityLevel.NORMAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void remove_from_wishlist_button_changes_back_to_normal_state_after_click(ProductData productData) {
        productData.getProduct().clickOnTheWishListButton();
        productData.getProduct().clickOnTheRemoveFromWishlistButton();
        assertFalse(productData.getProduct().isRemoveWishlistButtonDisplayed(), "After clicking 'Remove from Wishlist' button, the button should change back to its normal state, but it is still displayed.");
    }

    @Test(dataProviderClass = WishlistDataProvider.class, dataProvider = "wishlist product")
    @Owner("Bianca Stoica")
    @Description("Wishlist badge is not displayed after emptying wishlist.")
    @Severity(SeverityLevel.NORMAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void wishlist_badge_is_not_displayed_after_emptying_wishlist(ProductData productData) {
        productData.getProduct().clickOnTheWishListButton();
        productData.getProduct().clickOnTheRemoveFromWishlistButton();
        assertFalse(header.isWishlistBadgeVisible(), "After emptying the wishlist, the wishlist badge should not be visible, but it is still displayed.");
    }

    @Test(dataProviderClass = WishlistDataProvider.class, dataProvider = "wishlist product")
    @Owner("Bianca Stoica")
    @Description("Wishlist badge is not displayed after emptying wishlist.")
    @Severity(SeverityLevel.NORMAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void wishlist_badge_value_drops_down_after_removing_products_from_wishlist(ProductData productData) {
        productData.getProduct().clickOnTheWishListButton();
        productData.getProduct().clickOnTheWishListButton();
        productData.getProduct().clickOnTheWishListButton();
        productData.getProduct().clickOnTheRemoveFromWishlistButton();
        productData.getProduct().clickOnTheRemoveFromWishlistButton();
        String wishlistBadgeValue = header.getWishlistBadgeValue();
        assertEquals(wishlistBadgeValue, "1", "After removing 2 product from the wishlist, badge shows 1");
    }

    @Test(dataProviderClass = WishlistDataProvider.class, dataProvider = "wishlist product")
    @Owner("Bianca Stoica")
    @Description("User can remove product from wishlist page.")
    @Severity(SeverityLevel.NORMAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void user_can_remove_product_from_wishlist_page(ProductData productData) {
        productData.getProduct().clickOnTheWishListButton();
        header.clickOnTheWishListIcon();
        productData.getProduct().clickOnTheRemoveFromWishlistButton();
        assertTrue(wishlistPage.isWishlistPageEmpty(), "Wishlist page is empty.");
    }

    @Test(dataProviderClass = UsersAndWishlistDataProvider.class, dataProvider = "userAndWishlistProduct")
    @Owner("Bianca Stoica")
    @Description("Valid users can remove product from wishlist page.")
    @Severity(SeverityLevel.NORMAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void valid_users_can_remove_product_from_wishlist_page(ValidAccount account, ProductData productData) {
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        productData.getProduct().clickOnTheWishListButton();
        header.clickOnTheWishListIcon();
        productData.getProduct().clickOnTheRemoveFromWishlistButton();
        assertTrue(wishlistPage.isWishlistPageEmpty(), "Wishlist page is empty.");
    }

    @Test
    @Owner("Bianca Stoica")
    @Description("User can remove multiple products from wishlist page.")
    @Severity(SeverityLevel.NORMAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void user_can_remove_multiple_products_from_wishlist_page() {
        Product product = new Product("1");
        product.clickOnTheWishListButton();
        new Product("2");
        product.clickOnTheWishListButton();
        new Product("3");
        product.clickOnTheWishListButton();
        header.clickOnTheWishListIcon();
        assertFalse(wishlistPage.isWishlistPageEmpty(), "Wishlist should not be empty after adding products.");
        product.clickOnTheRemoveFromWishlistButton();
        product.clickOnTheRemoveFromWishlistButton();
        assertFalse(wishlistPage.isWishlistPageEmpty(), "Wishlist should not be empty after removing some products.");
    }

    @Test(dataProviderClass = UsersAndWishlistDataProvider.class, dataProvider = "userAndWishlistProducts")
    @Owner("Bianca Stoica")
    @Description("Valid users can add and remove all products from wishlist page.")
    @Severity(SeverityLevel.NORMAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void valid_users_add_and_remove_all_products_from_wishlist_page(ValidAccount account, List<ProductData> productDataList) {
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        for (ProductData currentProductData : productDataList) {
            Product currentProduct = currentProductData.getProduct();
            currentProduct.clickOnTheWishListButton();
            System.out.println("Added product to wishlist: " + currentProductData.getName());
        }
        header.clickOnTheWishListIcon();
        assertFalse(wishlistPage.isWishlistPageEmpty(), "Wishlist should not be empty after adding products.");
        for (ProductData currentProductData : productDataList) {
            Product currentProduct = currentProductData.getProduct();
            currentProduct.clickOnTheRemoveFromWishlistButton();
            System.out.println("Removed product from wishlist: " + currentProductData.getName());
        }
        assertTrue(wishlistPage.isWishlistPageEmpty(), "Wishlist should be empty after removing all products.");
    }

    @Test(dataProviderClass = WishlistDataProvider.class, dataProvider = "wishlist product")
    @Owner("Bianca Stoica")
    @Description("User can add product to cart from wishlist page.")
    @Severity(SeverityLevel.NORMAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void user_can_add_product_to_cart_from_wishlist(ProductData productData) {
        productData.getProduct().clickOnTheWishListButton();
        header.clickOnTheWishListIcon();
        wishlistPage.clickOnTheCartButton();
        header.clickOnTheShoppingBagIcon();
        assertTrue(header.isShoppingBadgeVisible(), "Product is added to cart from wishlist page.");
    }

    @Test(dataProviderClass = UsersAndWishlistDataProvider.class, dataProvider = "userAndWishlistProducts")
    @Owner("Bianca Stoica")
    @Description("Valid users can add products to cart from wishlist page.")
    @Severity(SeverityLevel.NORMAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void valid_users_can_add_product_to_cart_from_wishlist(ValidAccount account, List<ProductData> productDataList) {
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        for (ProductData currentProductData : productDataList) {
            Product currentProduct = currentProductData.getProduct();
            currentProduct.clickOnTheWishListButton();
            System.out.println("Added product to wishlist: " + currentProductData.getName());
        }
        header.clickOnTheWishListIcon();
        for (ProductData currentProductData : productDataList) {
            Product currentProduct = currentProductData.getProduct();
            currentProduct.clickOnTheProductCartIcon();
            System.out.println("Added product to cart: " + currentProductData.getName());
        }
        header.clickOnTheShoppingBagIcon();
        assertTrue(header.isShoppingBadgeVisible(), "Products are added to the cart from the wishlist page.");
    }


    @Test(dataProviderClass = UsersAndWishlistDataProvider.class, dataProvider = "userAndWishlistProduct")
    @Owner("Bianca Stoica")
    @Description("Product remains in wishlist after valid users log in.")
    @Severity(SeverityLevel.NORMAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void product_remains_in_wishlist_after_valid_users_log_in(ValidAccount account, ProductData productData) {
        productData.getProduct().clickOnTheWishListButton();
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        String wishlistBadgeValue = header.getWishlistBadgeValue();
        assertEquals(wishlistBadgeValue, "1", "After valid users log in, product is still in wishlist");
    }

    @Test(dataProviderClass = UsersAndWishlistDataProvider.class, dataProvider = "userAndWishlistProducts")
    @Owner("Bianca Stoica")
    @Description("Products remain in wishlist after valid users log in.")
    @Severity(SeverityLevel.NORMAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void products_remain_in_wishlist_after_valid_users_log_in(ValidAccount account, List<ProductData> productDataList) {
        for (ProductData currentProductData : productDataList) {
            Product currentProduct = currentProductData.getProduct();
            currentProduct.clickOnTheWishListButton();
            System.out.println("Added product to wishlist: " + currentProductData.getName());
        }
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        String wishlistBadgeValue = header.getWishlistBadgeValue();
        assertEquals(wishlistBadgeValue, "10", "After valid users log in, product is still in wishlist");
    }

}


