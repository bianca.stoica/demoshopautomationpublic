package org.fasttrackit;

import io.qameta.allure.*;
import org.fasttrackit.dataprovider.AuthenticationDataProvider;
import org.fasttrackit.dataprovider.UsersAndProductsDataProvider;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.List;

import static org.testng.Assert.*;

public class UsersAndProductsInteractionTest {
    Page page = new Page();
    Header header = new Header();
    CartPage cartPage = new CartPage();
    ModalDialog modal = new ModalDialog();

    @BeforeClass
    public void setup() {
        page.openHomePage();
    }

    @AfterMethod
    public void cleanup() {
        Footer footer = new Footer();
        footer.clickToReset();
        header.clickOnTheShoppingBagIcon();
    }

    @Test(dataProviderClass = UsersAndProductsDataProvider.class, dataProvider = "userAndProduct")
    @Owner("Bianca Stoica")
    @Description("Valid users can add a product to cart.")
    @Severity(SeverityLevel.CRITICAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void valid_users_can_add_product_to_cart(ValidAccount account, ProductData productData) {
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        productData.getProduct().clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        assertFalse(cartPage.isEmptyCartMessageDisplayed());
    }

    @Test(dataProviderClass = UsersAndProductsDataProvider.class, dataProvider = "userAndProducts")
    @Owner("Bianca Stoica")
    @Description("Valid users can add multiple products to cart.")
    @Severity(SeverityLevel.CRITICAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void valid_users_can_add_products_to_cart(ValidAccount account, List<ProductData> productDataList) {
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        for (ProductData currentProductData : productDataList) {
            Product currentProduct = currentProductData.getProduct();
            currentProduct.clickOnTheProductCartIcon();
            System.out.println("Added product to cart: " + currentProductData.getName());
        }
        header.clickOnTheCartIcon();
        assertFalse(cartPage.isEmptyCartMessageDisplayed());
    }

    @Test(dataProviderClass = UsersAndProductsDataProvider.class, dataProvider = "userAndProduct")
    @Owner("Bianca Stoica")
    @Description("Valid users can reduce the amount of a product in cart page.")
    @Severity(SeverityLevel.CRITICAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void valid_users_can_reduce_the_amount_of_a_product_in_cart_page(ValidAccount account, ProductData productData) {
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        productData.getProduct().clickOnTheProductCartIcon();
        productData.getProduct().clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        CartItem item = new CartItem(productData.getId());
        item.reduceAmount();
        assertEquals(item.getItemAmount(), "1");
    }

    @Test(dataProviderClass = UsersAndProductsDataProvider.class, dataProvider = "userAndProduct")
    @Owner("Bianca Stoica")
    @Description("Valid users can add product to cart from product card.")
    @Severity(SeverityLevel.CRITICAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void valid_users_can_add_product_to_cart_from_product_card(ValidAccount account, ProductData productData) {
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        productData.getProduct().clickOnTheProductCartIcon();
        System.out.println("Added product to cart: " + productData.getName());
        assertTrue(header.isShoppingBadgeVisible());
        String badgeValue = header.getShoppingCartBadgeValue();
        assertEquals(badgeValue, "1", "After adding 1 product to cart, badge shows 1");
    }

    @Test(dataProviderClass = UsersAndProductsDataProvider.class, dataProvider = "userAndProduct")
    @Owner("Bianca Stoica")
    @Description("Valid users can increment product amount in product card section.")
    @Severity(SeverityLevel.CRITICAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void valid_users_can_increment_product_amount_in_product_card_section(ValidAccount account, ProductData productData) {
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        productData.getProduct().clickOnTheProductCartIcon();
        productData.getProduct().clickOnTheProductCartIcon();
        assertTrue(header.isShoppingBadgeVisible());
        String badgeValue = header.getShoppingCartBadgeValue();
        assertEquals(badgeValue, "2", "After adding 2 product to cart, badge shows 2");
    }

    @Test(dataProviderClass = UsersAndProductsDataProvider.class, dataProvider = "userAndProduct")
    @Owner("Bianca Stoica")
    @Description("Valid users can delete a product from the cart page by clicking the trash can button.")
    @Severity(SeverityLevel.NORMAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void valid_users_can_delete_a_product_from_the_cart_page_by_clicking_the_trash_can_button(ValidAccount account, ProductData productData) {
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        productData.getProduct().clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        CartItem item = new CartItem(productData.getId());
        item.clickTrashCanButton();
        assertEquals(cartPage.getEmptyCartPageText(), "How about adding some products in your cart?");
    }

    @Test(dataProviderClass = UsersAndProductsDataProvider.class, dataProvider = "userAndProduct")
    @Owner("Bianca Stoica")
    @Description("Valid users can click on the 'continue shopping' button.")
    @Severity(SeverityLevel.CRITICAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void valid_users_can_click_on_the_continue_shopping_button(ValidAccount account, ProductData productData) {
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        productData.getProduct().clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheContinueShoppingButton();
        assertEquals(page.getPageTitle(), "Products", "Expected to return on the 'Product' page ");
    }

    @Test(dataProvider = "validCredentials", dataProviderClass = AuthenticationDataProvider.class)
    @Owner("Bianca Stoica")
    @Description("Deleting first product in cart removes last product for valid users.")
    @Severity(SeverityLevel.CRITICAL)
    @Issue("BUG-1")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void deleting_first_product_in_cart_removes_last_product_for_valid_users(ValidAccount account) {
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        Product product1 = new Product("2");
        product1.clickOnTheProductCartIcon();
        Product product2 = new Product("5");
        product2.clickOnTheProductCartIcon();
        Product product3 = new Product("8");
        product3.clickOnTheProductCartIcon();
        Product product4 = new Product("6");
        product4.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        CartItem item1 = new CartItem("2");
        item1.clickTrashCanButton();
        int remainingProductCount = cartPage.getProductCount();
        assertEquals(remainingProductCount, 3, "After deleting the first product, expected three products in the cart.");
    }

    @Test(dataProviderClass = UsersAndProductsDataProvider.class, dataProvider = "userAndProduct")
    @Owner("Bianca Stoica")
    @Description("Increase product amount for valid users and validate increased amount.")
    @Severity(SeverityLevel.CRITICAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void increase_product_amount_for_valid_users_and_validate_increased_amount(ValidAccount account, ProductData productData) {
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        productData.getProduct().clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        CartItem item = new CartItem(productData.getId());
        double initialTotal = cartPage.getTotalAmount();
        item.increaseAmount();
        double updatedTotal = cartPage.getTotalAmount();
        assertTrue(updatedTotal > initialTotal, "The total amount did not increase after adding the product. Initial total: $" + initialTotal + ", Updated total: $" + updatedTotal);
    }

    @Test(dataProviderClass = UsersAndProductsDataProvider.class, dataProvider = "userAndProduct")
    @Owner("Bianca Stoica")
    @Description("Reduce product amount for valid users and validate reduced amount.")
    @Severity(SeverityLevel.CRITICAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void reduce_product_amount_for_valid_users_and_validate_reduced_amount(ValidAccount account, ProductData productData) {
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        productData.getProduct().clickOnTheProductCartIcon();
        productData.getProduct().clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        CartItem item = new CartItem(productData.getId());
        double initialTotal = cartPage.getTotalAmount();
        item.reduceAmount();
        double updatedTotal = cartPage.getTotalAmount();
        assertTrue(updatedTotal < initialTotal, "The initial total amount din not reduce after removing a product");
    }

    @Test(dataProvider = "validCredentials", dataProviderClass = AuthenticationDataProvider.class)
    @Owner("Bianca Stoica")
    @Description("Valid users add multiple products to cart total and verify total price.")
    @Severity(SeverityLevel.CRITICAL)
    @Issue("BUG-2")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void valid_users_add_multiple_products_to_cart_total_and_verify_total_price(ValidAccount account) {
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        Product product1 = new Product("1");
        product1.clickOnTheProductCartIcon();
        header.clickOnTheShoppingBagIcon();
        Product product2 = new Product("3");
        product2.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        header.clickOnTheShoppingBagIcon();
        Product product3 = new Product("2");
        product3.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        double totalPrice = cartPage.getTotalAmount();
        assertEquals(totalPrice, "39.97", "Added 3 products in the cart, expected the total price to be 39.97 ");
    }

    @Test(dataProvider = "validCredentials", dataProviderClass = AuthenticationDataProvider.class)
    @Owner("Bianca Stoica")
    @Description("Valid users increase quantity for each product in cart and validate total price.")
    @Severity(SeverityLevel.CRITICAL)
    @Issue("BUG-3")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void valid_users_increase_quantity_for_each_product_in_cart_and_validate_total_price(ValidAccount account) {
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        Product product1 = new Product("1");
        product1.clickOnTheProductCartIcon();
        Product product2 = new Product("3");
        product2.clickOnTheProductCartIcon();
        Product product3 = new Product("2");
        product3.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        CartItem item1 = new CartItem("1");
        item1.increaseAmount();
        CartItem item2 = new CartItem("3");
        item2.increaseAmount();
        CartItem item3 = new CartItem("2");
        item3.increaseAmount();
        double totalPrice = cartPage.getTotalAmount();
        assertEquals(totalPrice, "79.94", "Increased quantity for each product in cart, expected total price to be 79.94 ");
    }

    @Test(dataProviderClass = UsersAndProductsDataProvider.class, dataProvider = "userAndProduct")
    @Owner("Bianca Stoica")
    @Description("Adding product to cart and increasing quantity displays correct price for valid users.")
    @Severity(SeverityLevel.CRITICAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void adding_product_to_cart_and_increasing_quantity_displays_correct_price_for_valid_users(ValidAccount account, ProductData productData) {
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        productData.getProduct().clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        CartItem item = new CartItem(productData.getId());
        String initialPriceString = item.getAmountControlPrice();
        double initialPrice = item.parsePriceString(initialPriceString);
        item.increaseAmount();
        String updatedPriceString = item.getAmountControlPrice();
        double updatedPrice = item.parsePriceString(updatedPriceString);
        Assert.assertEquals(updatedPrice, initialPrice * 2, "The updated price is not double the initial price");
    }

    @Test(dataProviderClass = UsersAndProductsDataProvider.class, dataProvider = "userAndProduct")
    @Owner("Bianca Stoica")
    @Description("Product remains in cart after valid users log in.")
    @Severity(SeverityLevel.NORMAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void product_remains_in_cart_after_valid_users_log_in(ValidAccount account, ProductData productData) {
        productData.getProduct().clickOnTheProductCartIcon();
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        String badgeValue = header.getShoppingCartBadgeValue();
        assertEquals(badgeValue, "1", "After valid user is logged in, product is still present in cart");
    }

    @Test(dataProvider = "validCredentials", dataProviderClass = AuthenticationDataProvider.class)
    @Owner("Bianca Stoica")
    @Description("Validate inability of valid users to empty cart by deleting from first to last product order.")
    @Severity(SeverityLevel.CRITICAL)
    @Issue("BUG-4")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void validate_inability_of_valid_users_to_empty_cart_by_deleting_from_first_to_last_product_order(ValidAccount account) {
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        Product product1 = new Product("2");
        product1.clickOnTheProductCartIcon();
        Product product2 = new Product("5");
        product2.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        CartItem item1 = new CartItem("2");
        item1.clickTrashCanButton();
        assertFalse(cartPage.isProductPresentInCart("5"), "Product is present in the cart");
        assertNotEquals(cartPage.getEmptyCartPageText(), "How about adding some products in your cart?");
    }

    @Test(dataProvider = "validCredentials", dataProviderClass = AuthenticationDataProvider.class)
    @Owner("Bianca Stoica")
    @Description("Validating the product details in the cart with valid users.")
    @Severity(SeverityLevel.CRITICAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void validating_the_product_details_in_the_cart_with_valid_users(ValidAccount account) {
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        Product product1 = new Product("1");
        product1.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        CartItem cartItem = new CartItem("1");
        String cartProductName = cartItem.getProductName();
        String cartProductPrice = cartItem.getProductPrice();
        String cartProductQuantity = cartItem.getItemAmount();

        assertEquals(cartProductName, "Awesome Granite Chips", "Product names do not match");
        assertEquals(cartProductPrice, "$15.99", "Product price do not match");
        assertEquals(cartProductQuantity, "1", "Product quantities do not match");
    }
}
