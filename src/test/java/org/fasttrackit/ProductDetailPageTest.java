package org.fasttrackit;


import io.qameta.allure.*;
import org.fasttrackit.dataprovider.AuthenticationDataProvider;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import static org.testng.Assert.*;

public class ProductDetailPageTest {

    Page page = new Page();
    Header header = new Header();
    Footer footer = new Footer();
    ProductDetailPage productDetailPage = new ProductDetailPage();
    ModalDialog modal = new ModalDialog();

    @BeforeClass
    public void setup() {
        page.openHomePage();
    }

    @AfterMethod
    public void clean_up() {
        footer.clickToReset();
        header.clickOnTheShoppingBagIcon();
    }

    @Test
    @Owner("Bianca Stoica")
    @Description("Verifying that the product description is displayed.")
    @Severity(SeverityLevel.NORMAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void verify_if_the_product_description_is_displayed() {
        ProductCards productCards = new ProductCards();
        productCards.clickOnTheProductLink(3);
        assertTrue(productDetailPage.getProductDescription(), "Product description is not displayed");
    }

    @Test(dataProvider = "validCredentials", dataProviderClass = AuthenticationDataProvider.class)
    @Owner("Bianca Stoica")
    @Description("Verifying that the product description is displayed for valid users.")
    @Severity(SeverityLevel.NORMAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void verify_if_the_product_description_is_displayed_for_valid_users(ValidAccount account) {
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        ProductCards productCards = new ProductCards();
        productCards.clickOnTheProductLink(4);
        assertTrue(productDetailPage.getProductDescription(), "Product description is not displayed");
    }

    @Test
    @Owner("Bianca Stoica")
    @Description("Verifying that the product picture is displayed.")
    @Severity(SeverityLevel.NORMAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void verify_if_the_product_picture_is_displayed() {
        ProductCards productCards = new ProductCards();
        productCards.clickOnTheProductLink(3);
        assertTrue(productDetailPage.getProductImage(), "Product image is not displayed");
    }

    @Test(dataProvider = "validCredentials", dataProviderClass = AuthenticationDataProvider.class)
    @Owner("Bianca Stoica")
    @Description("Verifying that the product picture is displayed for_valid_users.")
    @Severity(SeverityLevel.NORMAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void verify_if_the_product_picture_is_displayed_valid_users(ValidAccount account) {
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        ProductCards productCards = new ProductCards();
        productCards.clickOnTheProductLink(3);
        assertTrue(productDetailPage.getProductImage(), "Product image is not displayed");
    }

    @Test
    @Owner("Bianca Stoica")
    @Description("Testing product title consistency on product page.")
    @Severity(SeverityLevel.NORMAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void testing_product_title_consistency_on_product_page() {
        ProductCards productCards = new ProductCards();
        productCards.clickOnTheProductLink(0);
        assertEquals(productDetailPage.getProductTitle(), "Awesome Granite Chips", "Expected to see title 'Awesome Granite Chips' on product page");
    }

    @Test(dataProvider = "validCredentials", dataProviderClass = AuthenticationDataProvider.class)
    @Owner("Bianca Stoica")
    @Description("Testing product title consistency on product page for valid users.")
    @Severity(SeverityLevel.NORMAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void testing_product_title_consistency_on_product_page_for_valid_users(ValidAccount account) {
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        ProductCards productCards = new ProductCards();
        productCards.clickOnTheProductLink(0);
        assertEquals(productDetailPage.getProductTitle(), "Awesome Granite Chips", "Expected to see title 'Awesome Granite Chips' on product page");
    }

    @Test
    @Owner("Bianca Stoica")
    @Description("User can add product in cart from product description.")
    @Severity(SeverityLevel.NORMAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void user_can_add_product_in_the_cart_from_product_description_page() {
        ProductCards productCards = new ProductCards();
        productCards.clickOnTheProductLink(6);
        productDetailPage.addProductToCart();
        String badgeValue = header.getShoppingCartBadgeValue();
        assertEquals(badgeValue, "1", "After adding 1 product to cart, badge shows 1");
    }

    @Test(dataProvider = "validCredentials", dataProviderClass = AuthenticationDataProvider.class)
    @Owner("Bianca Stoica")
    @Description("Valid users can add product in cart from product description.")
    @Severity(SeverityLevel.NORMAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void valid_users_can_add_product_in_the_cart_from_product_description_page(ValidAccount account) {
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        ProductCards productCards = new ProductCards();
        productCards.clickOnTheProductLink(6);
        productDetailPage.addProductToCart();
        String badgeValue = header.getShoppingCartBadgeValue();
        assertEquals(badgeValue, "1", "After adding 1 product to cart, badge shows 1");
    }

    @Test
    @Owner("Bianca Stoica")
    @Description("User can add product in wishlist from product description page.")
    @Severity(SeverityLevel.NORMAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void user_can_add_product_in_the_wishlist_from_product_description_page() {
        ProductCards productCards = new ProductCards();
        productCards.clickOnTheProductLink(6);
        productDetailPage.addProductToWishlist();
        String wishlistBadgeValue = header.getWishlistBadgeValue();
        assertEquals(wishlistBadgeValue, "1", "After adding 1 product in wishlist, badge shows 1");
    }

    @Test(dataProvider = "validCredentials", dataProviderClass = AuthenticationDataProvider.class)
    @Owner("Bianca Stoica")
    @Description("Valid users can add product in wishlist from product description page.")
    @Severity(SeverityLevel.NORMAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void valid_users_can_add_product_in_the_wishlist_from_product_description_page(ValidAccount account) {
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        ProductCards productCards = new ProductCards();
        productCards.clickOnTheProductLink(6);
        productDetailPage.addProductToWishlist();
        String wishlistBadgeValue = header.getWishlistBadgeValue();
        assertEquals(wishlistBadgeValue, "1", "After adding 1 product in wishlist, badge shows 1");
    }

}