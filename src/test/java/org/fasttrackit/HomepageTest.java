package org.fasttrackit;

import io.qameta.allure.*;
import org.fasttrackit.dataprovider.AuthenticationDataProvider;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import static org.testng.Assert.*;

public class HomepageTest {
    Page page = new Page();
    ModalDialog modalDialog = new ModalDialog();
    Footer footer = new Footer();
    Header header = new Header();
    ModalDialog modal = new ModalDialog();


    @BeforeClass
    public void setup() {
        page.openHomePage();
    }

    @AfterMethod
    public void clean_up() {
        footer.clickToReset();
    }

    @Test
    @Owner("Bianca Stoica")
    @Description("User can click on the 'Help' icon.")
    @Severity(SeverityLevel.NORMAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void user_can_click_on_the_Help_icon() {
        page.clickOnTheHelpButton();
        String modalTitle = modalDialog.getModalTitle();
        assertEquals(modalTitle, "Help", "Expected modal title to be 'Help'");
        modalDialog.clickOnTheCloseButton();
    }

    @Test(dataProvider = "validCredentials", dataProviderClass = AuthenticationDataProvider.class)
    @Owner("Bianca Stoica")
    @Description("Valid users can click on the 'Help' icon.")
    @Severity(SeverityLevel.NORMAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void valid_users_can_click_on_the_Help_icon(ValidAccount account) {
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        page.clickOnTheHelpButton();
        String modalTitle = modalDialog.getModalTitle();
        assertEquals(modalTitle, "Help", "Expected modal title to be 'Help'");
        modalDialog.clickOnTheCloseButton();
    }
    @Test
    @Owner("Bianca Stoica")
    @Description("Checking if homepage title is displayed.")
    @Severity(SeverityLevel.NORMAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void checking_if_homepage_title_is_displayed() {
        assertEquals(page.getPageTitle(), "Products", "Expected homepage name to be 'Products'.");
    }

}