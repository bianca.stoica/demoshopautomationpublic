package org.fasttrackit;

import io.qameta.allure.*;
import org.fasttrackit.dataprovider.SearchBarDataProvider;
import org.fasttrackit.dataprovider.UserAndSearchBarDataProvider;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.*;


public class SearchBarTest {

    Page page = new Page();

    Footer footer = new Footer();
    SearchPage searchPage = new SearchPage();
    Header header = new Header();
    ModalDialog modal = new ModalDialog();

    @BeforeClass
    public void setup() {
        page.openHomePage();
    }

    @AfterMethod
    public void clean_up() {
        footer.clickToReset();
    }

    @Test
    @Owner("Bianca Stoica")
    @Description("User can search products with keyword.")
    @Severity(SeverityLevel.CRITICAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void user_can_search_products_with_keyword() {
        searchPage.clickOnTheSearchBox();
        searchPage.typeInKeyword("Awesome");
        searchPage.clickOnTheSearchButton();
        assertTrue(searchPage.areSearchResultsContainingKeyword("Awesome"), "Expected keyword 'Awesome' in the search results , but not found");
    }

    @Test
    @Owner("Bianca Stoica")
    @Description("User can search products with price number.")
    @Severity(SeverityLevel.CRITICAL)
    @Issue("BUG-10")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void user_can_search_products_with_price_number() {
        searchPage.clickOnTheSearchBox();
        searchPage.typeInKeyword("15.99");
        searchPage.clickOnTheSearchButton();
        assertTrue(searchPage.areSearchResultsContainingPrice(Double.parseDouble("15.99")), "Expected to see products with 15.99 price, but not found");
    }

    @Test
    @Owner("Bianca Stoica")
    @Description("User can search products with lower and upper case letter word.")
    @Severity(SeverityLevel.NORMAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void user_can_search_products_with_lower_and_upper_case_letter_word() {
        searchPage.clickOnTheSearchBox();
        searchPage.typeInKeyword("MoUsE");
        searchPage.clickOnTheSearchButton();
        assertTrue(searchPage.areSearchResultsContainingKeyword("Mouse"), "Expected keyword 'Mouse' in the search results , but not found");
    }

    @Test
    @Owner("Bianca Stoica")
    @Description("User can search products with upper case letter word.")
    @Severity(SeverityLevel.NORMAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void user_can_search_products_with_upper_case_word() {
        searchPage.clickOnTheSearchBox();
        searchPage.typeInKeyword("PIZZA");
        searchPage.clickOnTheSearchButton();
        assertTrue(searchPage.areSearchResultsContainingKeyword("Pizza"), "Expected keyword 'Pizza' in the search results , but not found");
    }

    @Test
    @Owner("Bianca Stoica")
    @Description("User can search products with lower case letter word.")
    @Severity(SeverityLevel.NORMAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void user_can_search_products_with_lower_case_word() {
        searchPage.clickOnTheSearchBox();
        searchPage.typeInKeyword("wooden");
        searchPage.clickOnTheSearchButton();
        assertTrue(searchPage.areSearchResultsContainingKeyword("Wooden"), "Expected keyword 'Wooden' in the search results , but not found");
    }

    @Test
    @Owner("Bianca Stoica")
    @Description("User can search products with one letter.")
    @Severity(SeverityLevel.NORMAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void user_can_search_products_with_one_letter() {
        searchPage.clickOnTheSearchBox();
        searchPage.typeInKeyword("g");
        searchPage.clickOnTheSearchButton();
        assertTrue(searchPage.areSearchResultsContainingKeyword("g"), "Expected keyword 'g' in the search results , but not found");
    }

    @Test
    @Owner("Bianca Stoica")
    @Description("User can search products with prefix.")
    @Severity(SeverityLevel.NORMAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void user_can_search_products_with_prefix() {
        searchPage.clickOnTheSearchBox();
        searchPage.typeInKeyword("awe");
        searchPage.clickOnTheSearchButton();
        assertTrue(searchPage.areSearchResultsContainingKeyword("Awesome"), "Expected keyword 'Awesome' in the search results , but not found");

    }

    @Test
    @Owner("Bianca Stoica")
    @Description("User can search products with suffix.")
    @Severity(SeverityLevel.NORMAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void user_can_search_products_with_suffix() {
        searchPage.clickOnTheSearchBox();
        searchPage.typeInKeyword("ete");
        searchPage.clickOnTheSearchButton();
        assertTrue(searchPage.areSearchResultsContainingKeyword("Concrete"), "Expected keyword 'Concrete' in the search results , but not found");
    }

    @Test
    @Owner("Bianca Stoica")
    @Description("User can search products with substring.")
    @Severity(SeverityLevel.NORMAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void user_can_search_products_with_substring() {
        searchPage.clickOnTheSearchBox();
        searchPage.typeInKeyword("ee");
        searchPage.clickOnTheSearchButton();
        assertTrue(searchPage.areSearchResultsContainingKeyword("Steel"), "Expected keyword 'Steel' in the search results , but not found");
    }

    @Test
    @Owner("Bianca Stoica")
    @Description("User cannot search products with empty query.")
    @Severity(SeverityLevel.NORMAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void user_cannot_search_with_empty_query() {
        searchPage.clickOnTheSearchBox();
        searchPage.typeInKeyword("");
        searchPage.clickOnTheSearchButton();
        assertFalse(searchPage.areSearchResultsContainingKeyword("Any"), "Expected no search results for an empty query, but found some.");
    }

    @Test
    @Owner("Bianca Stoica")
    @Description("User cannot search products with nonexistent query.")
    @Severity(SeverityLevel.NORMAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void user_cannot_search_products_with_nonexistent_query() {
        searchPage.clickOnTheSearchBox();
        searchPage.typeInKeyword("Burger");
        searchPage.clickOnTheSearchButton();
        assertFalse(searchPage.areSearchResultsContainingKeyword("Burger"), "Expected no search results for a nonexistent keyword, but found some.");
    }

    @Test
    @Owner("Bianca Stoica")
    @Description("User can search products with special characters.")
    @Severity(SeverityLevel.NORMAL)
    @Issue("BUG-11")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void user_can_search_with_special_characters() {
        searchPage.clickOnTheSearchBox();
        searchPage.typeInKeyword("!@#$%^&*()");
        searchPage.clickOnTheSearchButton();
        assertTrue(searchPage.areSearchResultsContainingKeyword("!@#$%^&*()"), "Expected search results for a query with special characters, but found none.");
    }

    @Test
    @Owner("Bianca Stoica")
    @Description("User can search products with multiple keywords.")
    @Severity(SeverityLevel.NORMAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void user_can_search_with_multiple_keywords() {
        searchPage.clickOnTheSearchBox();
        searchPage.typeInKeyword("Practical  Awesome");
        searchPage.clickOnTheSearchButton();
        assertTrue(searchPage.areSearchResultsContainingKeyword("Practical") && searchPage.areSearchResultsContainingKeyword("Awesome"), "Expected search results for multiple keywords, but not found.");
    }

    @Test(dataProvider = "validUserAndValidSearchQuery", dataProviderClass = UserAndSearchBarDataProvider.class)
    @Owner("Bianca Stoica")
    @Description("Valid users can search products with valid search query.")
    @Severity(SeverityLevel.NORMAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void valid_users_can_search_products_with_valid_search_query(ValidAccount account, SearchQuery searchQuery) {
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        searchPage.clickOnTheSearchBox();
        searchPage.typeInKeyword(String.valueOf(searchQuery));
        searchPage.clickOnTheSearchButton();
        assertTrue(searchPage.areSearchResultsContainingKeyword(String.valueOf(searchQuery)), "Expected keyword '" + searchQuery + "' in the search results, but not found");
    }

    @Test(dataProvider = "validUserAndInvalidSearchQuery", dataProviderClass = UserAndSearchBarDataProvider.class)
    @Owner("Bianca Stoica")
    @Description("Valid users can search products with invalid search query.")
    @Severity(SeverityLevel.NORMAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void valid_users_can_search_products_with_invalid_search_query(ValidAccount account, SearchQuery searchQuery) {
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        searchPage.clickOnTheSearchBox();
        searchPage.typeInKeyword(String.valueOf(searchQuery));
        searchPage.clickOnTheSearchButton();
        assertTrue(searchPage.areSearchResultsContainingKeyword(String.valueOf(searchQuery)), "Expected keyword '" + searchQuery + "' in the search results, but not found");
    }

}





