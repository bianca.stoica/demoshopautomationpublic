package org.fasttrackit;

import io.qameta.allure.*;
import org.fasttrackit.dataprovider.AuthenticationDataProvider;
import org.fasttrackit.dataprovider.UsersAndInformationPageDataProvider;
import org.fasttrackit.dataprovider.YourInformationDataProvider;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.*;


public class YourInformationTest {
    Page page = new Page();
    Header header = new Header();
    Footer footer = new Footer();
    CartPage cartPage = new CartPage();
    InformationPage informationPage = new InformationPage();
    ModalDialog modal = new ModalDialog();

    @BeforeClass
    public void setup() {
        page.openHomePage();
    }

    @AfterMethod
    public void cleanup() {
        footer.clickToReset();
        header.clickOnTheShoppingBagIcon();
    }

    @Test(dataProviderClass = YourInformationDataProvider.class, dataProvider = "validInfoProvider")
    @Owner("Bianca Stoica")
    @Description("User fills in information fields with valid information.")
    @Severity(SeverityLevel.CRITICAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void user_fills_in_information_fields_with_valid_info(YourInformation yourInformation) {
        Product product = new Product("5");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheCheckoutButton();
        informationPage.clickOnTheFirstNameField();
        informationPage.typeInFirstName(yourInformation.getFirstName());
        informationPage.clickOnTheLastNameField();
        informationPage.typeInLastName(yourInformation.getLastName());
        informationPage.clickOnTheAddressField();
        informationPage.typeInAddress(yourInformation.getAddress());
        informationPage.clickOnTheContinueCheckoutButton();
        assertFalse(informationPage.isErrorMessageDisplayed(), "Error message is displayed for valid information.");
    }

    @Test(dataProviderClass = YourInformationDataProvider.class, dataProvider = "invalidInfoProvider")
    @Owner("Bianca Stoica")
    @Description("User fills in information fields with invalid information.")
    @Severity(SeverityLevel.CRITICAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void user_fills_in_information_fields_with_invalid_info(YourInformation yourInformation) {
        Product product = new Product("5");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheCheckoutButton();
        informationPage.clickOnTheFirstNameField();
        informationPage.typeInFirstName(yourInformation.getFirstName());
        informationPage.clickOnTheLastNameField();
        informationPage.typeInLastName(yourInformation.getLastName());
        informationPage.clickOnTheAddressField();
        informationPage.typeInAddress(yourInformation.getAddress());
        informationPage.clickOnTheContinueCheckoutButton();
        assertTrue(informationPage.isErrorMessageDisplayed(), "Error message is displayed for invalid information.");
    }

    @Test(dataProviderClass = UsersAndInformationPageDataProvider.class, dataProvider = "validUserProductAndInvalidInfo")
    @Owner("Bianca Stoica")
    @Description("Valid users fill in information fields with valid information and can go to order summary page.")
    @Severity(SeverityLevel.CRITICAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void valid_users_fill_in_information_fields_with_valid_info_and_can_go_to_order_summary_page(ValidAccount account, ProductData productData, YourInformation yourInformation) {
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        productData.getProduct().clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheCheckoutButton();
        informationPage.clickOnTheFirstNameField();
        informationPage.typeInFirstName(yourInformation.getFirstName());
        informationPage.clickOnTheLastNameField();
        informationPage.typeInLastName(yourInformation.getLastName());
        informationPage.clickOnTheAddressField();
        informationPage.typeInAddress(yourInformation.getAddress());
        informationPage.clickOnTheContinueCheckoutButton();
        assertFalse(informationPage.isErrorMessageDisplayed(), "Error message is displayed for valid information.");
    }

    @Test(dataProviderClass = UsersAndInformationPageDataProvider.class, dataProvider = "validUserProductAndInvalidInfo")
    @Owner("Bianca Stoica")
    @Description("Valid users can not fill in information fields with invalid info and go to order summary page.")
    @Severity(SeverityLevel.CRITICAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void valid_users_can_not_fill_in_information_fields_with_invalid_info(ValidAccount account, ProductData productData, YourInformation yourInformation) {
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        productData.getProduct().clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheCheckoutButton();
        informationPage.clickOnTheFirstNameField();
        informationPage.typeInFirstName(yourInformation.getFirstName());
        informationPage.clickOnTheLastNameField();
        informationPage.typeInLastName(yourInformation.getLastName());
        informationPage.clickOnTheAddressField();
        informationPage.typeInAddress(yourInformation.getAddress());
        informationPage.clickOnTheContinueCheckoutButton();
        assertTrue(informationPage.isErrorMessageDisplayed(), "Error message not displayed for invalid information");
    }

    @Test()
    @Owner("Bianca Stoica")
    @Description("User can pick credit card payment option.")
    @Severity(SeverityLevel.CRITICAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void user_can_pick_credit_card_payment_option() {
        Product product = new Product("1");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheCheckoutButton();
        assertFalse(informationPage.isCreditCardOptionDisabled(), "Credit card option is disabled.");
    }

    @Test(dataProviderClass = AuthenticationDataProvider.class, dataProvider = "validCredentials")
    @Owner("Bianca Stoica")
    @Description("Valid users can pick credit card payment option.")
    @Severity(SeverityLevel.CRITICAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void valid_users_can_pick_credit_card_payment_option(ValidAccount account) {
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        Product product = new Product("2");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheCheckoutButton();
        assertFalse(informationPage.isCreditCardOptionDisabled(), "Credit card option is disabled.");
    }

    @Test
    @Owner("Bianca Stoica")
    @Description("User can pick pay pal payment option.")
    @Severity(SeverityLevel.CRITICAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void user_can_pick_pay_pal_payment_option() {
        Product product = new Product("1");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheCheckoutButton();
        assertFalse(informationPage.isPayPalOptionDisabled(), "PayPal option is disabled.");
    }

    @Test(dataProviderClass = AuthenticationDataProvider.class, dataProvider = "validCredentials")
    @Owner("Bianca Stoica")
    @Description("Valid users can pick pay pal payment option.")
    @Severity(SeverityLevel.CRITICAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void valid_users_can_pick_pay_pal_payment_option(ValidAccount account) {
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        Product product = new Product("2");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheCheckoutButton();
        assertFalse(informationPage.isPayPalOptionDisabled(), "PayPal option is disabled.");
    }


}

