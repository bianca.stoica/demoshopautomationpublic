package org.fasttrackit;


import io.qameta.allure.*;
import org.fasttrackit.dataprovider.ProductsDataProvider;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


import static org.testng.Assert.*;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;


public class CartManagementTest {
    Page page = new Page();
    Header header = new Header();
    CartPage cartPage = new CartPage();


    @BeforeClass
    public void setup() {
        page.openHomePage();
    }

    @AfterMethod
    public void cleanup() {
        Footer footer = new Footer();
        footer.clickToReset();
        header.clickOnTheShoppingBagIcon();
    }

    @Test
    @Owner("Bianca Stoica")
    @Description("User navigates to cart page, expected empty cart page message displayed.")
    @Severity(SeverityLevel.CRITICAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void when_user_navigates_to_cart_page_empty_cart_page_message_is_displayed() {
        header.clickOnTheCartIcon();
        assertEquals(cartPage.getEmptyCartPageText(), "How about adding some products in your cart?");
    }

    @Test(dataProviderClass = ProductsDataProvider.class, dataProvider = "products")
    @Owner("Bianca Stoica")
    @Description("User adds one product to cart, empty cart message should not be displayed.")
    @Severity(SeverityLevel.CRITICAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void adding_one_product_to_cart_empty_cart_message_is_not_shown(ProductData productData) {
        productData.getProduct().clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        assertFalse(cartPage.isEmptyCartMessageDisplayed());

    }

    @Test(dataProviderClass = ProductsDataProvider.class, dataProvider = "products")
    @Owner("Bianca Stoica")
    @Description("User can increment amount of product in cart page.")
    @Severity(SeverityLevel.CRITICAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void user_can_increment_amount_of_a_product_in_cart_page(ProductData productData) {
        productData.getProduct().clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        CartItem item = new CartItem(productData.getId());
        item.increaseAmount();
        assertEquals(item.getItemAmount(), "2");

    }

    @Test(dataProviderClass = ProductsDataProvider.class, dataProvider = "products")
    @Owner("Bianca Stoica")
    @Description("User can reduce amount of product in cart page")
    @Severity(SeverityLevel.CRITICAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void user_can_reduce_amount_of_a_product_in_cart_page(ProductData productData) {
        productData.getProduct().clickOnTheProductCartIcon();
        productData.getProduct().clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        CartItem item = new CartItem(productData.getId());
        item.reduceAmount();
        assertEquals(item.getItemAmount(), "1");
    }

    @Test(dataProviderClass = ProductsDataProvider.class, dataProvider = "products")
    @Owner("Bianca Stoica")
    @Description("User can add product to cart from product card.")
    @Severity(SeverityLevel.CRITICAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void user_can_add_product_to_cart_from_product_card(ProductData productData) {
        productData.getProduct().clickOnTheProductCartIcon();
        System.out.println("Added product to cart: " + productData.getName());
        assertTrue(header.isShoppingBadgeVisible());
        String badgeValue = header.getShoppingCartBadgeValue();
        assertEquals(badgeValue, "1", "After adding 1 product to cart, badge shows 1");
    }

    @Test(dataProviderClass = ProductsDataProvider.class, dataProvider = "products")
    @Owner("Bianca Stoica")
    @Description("User can increment product amount in product card section")
    @Severity(SeverityLevel.CRITICAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void user_can_increment_product_amount_in_product_card_section(ProductData productData) {
        productData.getProduct().clickOnTheProductCartIcon();
        productData.getProduct().clickOnTheProductCartIcon();
        assertTrue(header.isShoppingBadgeVisible());
        String badgeValue = header.getShoppingCartBadgeValue();
        assertEquals(badgeValue, "2", "After adding 2 product to cart, badge shows 2");
    }

    @Test(dataProviderClass = ProductsDataProvider.class, dataProvider = "products")
    @Owner("Bianca Stoica")
    @Description("User can delete product from cart, by clicking the trash can button.")
    @Severity(SeverityLevel.NORMAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void user_can_delete_a_product_from_the_cart_page_by_clicking_the_trash_can_button(ProductData productData) {
        productData.getProduct().clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        CartItem item = new CartItem(productData.getId());
        item.clickTrashCanButton();
        assertEquals(cartPage.getEmptyCartPageText(), "How about adding some products in your cart?");
    }

    @Test(dataProviderClass = ProductsDataProvider.class, dataProvider = "products")
    @Owner("Bianca Stoica")
    @Description("User can click on the 'Continue shopping' button")
    @Severity(SeverityLevel.CRITICAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void user_can_click_on_the_continue_shopping_button(ProductData productData) {
        productData.getProduct().clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        cartPage.clickOnTheContinueShoppingButton();
        assertEquals(page.getPageTitle(), "Products", "Expected to return on the 'Product' page ");
    }

    @Test
    @Owner("Bianca Stoica")
    @Description("User deletes first product in cart page removes last product.")
    @Severity(SeverityLevel.CRITICAL)
    @Issue("BUG-5")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void deleting_first_product_in_cart_removes_last_product() {
        Product product1 = new Product("2");
        product1.clickOnTheProductCartIcon();
        Product product2 = new Product("5");
        product2.clickOnTheProductCartIcon();
        Product product3 = new Product("8");
        product3.clickOnTheProductCartIcon();
        Product product4 = new Product("6");
        product4.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        CartItem item1 = new CartItem("2");
        item1.clickTrashCanButton();
        int remainingProductCount = cartPage.getProductCount();
        assertEquals(remainingProductCount, 3, "After deleting the first product, expected three products in the cart.");
    }


    @Test(dataProviderClass = ProductsDataProvider.class, dataProvider = "products")
    @Owner("Bianca Stoica")
    @Description("User increases product amount and validates the amount was increased.")
    @Severity(SeverityLevel.CRITICAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void user_increases_product_amount_and_validates_increased_amount(ProductData productData) {
        productData.getProduct().clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        CartItem item = new CartItem(productData.getId());
        double initialTotal = cartPage.getTotalAmount();
        item.increaseAmount();
        double updatedTotal = cartPage.getTotalAmount();
        assertTrue(updatedTotal > initialTotal, "The total amount did not increase after adding the product. Initial total: $" + initialTotal + ", Updated total: $" + updatedTotal);
    }

    @Test(dataProviderClass = ProductsDataProvider.class, dataProvider = "products")
    @Owner("Bianca Stoica")
    @Description("User decreases product amount and validates the amount was reduced.")
    @Severity(SeverityLevel.CRITICAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void user_decreases_product_amount_and_validates_reduced_amount(ProductData productData) {
        productData.getProduct().clickOnTheProductCartIcon();
        productData.getProduct().clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        CartItem item = new CartItem(productData.getId());
        double initialTotal = cartPage.getTotalAmount();
        item.reduceAmount();
        double updatedTotal = cartPage.getTotalAmount();
        assertTrue(updatedTotal < initialTotal, "The initial total amount din not reduce after removing a product");
    }

    @Test
    @Owner("Bianca Stoica")
    @Description("User adds multiple products to cart and verifies the total amount is correct.")
    @Severity(SeverityLevel.CRITICAL)
    @Issue("BUG-6")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void user_add_multiple_products_to_cart_total_and_verifies_correct_amount() {
        Product product1 = new Product("1");
        product1.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        header.clickOnTheShoppingBagIcon();
        Product product2 = new Product("3");
        product2.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        header.clickOnTheShoppingBagIcon();
        Product product3 = new Product("2");
        product3.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        double totalPrice = cartPage.getTotalAmount();
        assertEquals(totalPrice, "39.97", "Added 3 products in the cart, expected the total price to be 39.97 ");
    }

    @Test
    @Owner("Bianca Stoica")
    @Description("User increases quantity for every product in cart and validates the total price.")
    @Severity(SeverityLevel.CRITICAL)
    @Issue("BUG-7")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void user_increases_quantity_for_each_product_in_cart_and_validates_total_price() {
        Product product1 = new Product("1");
        product1.clickOnTheProductCartIcon();
        Product product2 = new Product("3");
        product2.clickOnTheProductCartIcon();
        Product product3 = new Product("2");
        product3.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        CartItem item1 = new CartItem("1");
        item1.increaseAmount();
        CartItem item2 = new CartItem("3");
        item2.increaseAmount();
        CartItem item3 = new CartItem("2");
        item3.increaseAmount();
        double totalPrice = cartPage.getTotalAmount();
        assertEquals(totalPrice, "79.94", "Increased quantity for each product in cart, expected total price to be 79.94 ");
    }

    @Test(dataProviderClass = ProductsDataProvider.class, dataProvider = "products")
    @Owner("Bianca Stoica")
    @Description("User doubles the quantity of a product expecting the price to be doubled .")
    @Severity(SeverityLevel.CRITICAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void user_doubles_quantity_of_product_expecting_price_to_be_doubled(ProductData productData) {
        productData.getProduct().clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        CartItem item = new CartItem(productData.getId());
        String initialPriceString = item.getAmountControlPrice();
        double initialPrice = item.parsePriceString(initialPriceString);
        item.increaseAmount();
        String updatedPriceString = item.getAmountControlPrice();
        double updatedPrice = item.parsePriceString(updatedPriceString);
        Assert.assertEquals(updatedPrice, initialPrice * 2, "The updated price is not double the initial price");
    }

    @Test
    @Owner("Bianca Stoica")
    @Description("Validate inability of user to empty cart by deleting from first to last product order.")
    @Severity(SeverityLevel.CRITICAL)
    @Issue("BUG-8")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void validate_inability_of_user_to_empty_cart_by_deleting_from_first_to_last_product_order() {
        Product product1 = new Product("2");
        product1.clickOnTheProductCartIcon();
        Product product2 = new Product("5");
        product2.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        CartItem item1 = new CartItem("2");
        item1.clickTrashCanButton();
        assertFalse(cartPage.isProductPresentInCart("5"), "Product is present in the cart");
        assertNotEquals(cartPage.getEmptyCartPageText(), "How about adding some products in your cart?");
    }

    @Test
    @Owner("Bianca Stoica")
    @Description("Validating products details in the cart page .")
    @Severity(SeverityLevel.CRITICAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void validating_the_product_details_in_the_cart() {
        Product product1 = new Product("1");
        product1.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        CartItem cartItem = new CartItem("1");
        String cartProductName = cartItem.getProductName();
        String cartProductPrice = cartItem.getProductPrice();
        String cartProductQuantity = cartItem.getItemAmount();

        assertEquals(cartProductName, "Awesome Granite Chips", "Product names do not match");
        assertEquals(cartProductPrice, "$15.99", "Product price do not match");
        assertEquals(cartProductQuantity, "1", "Product quantities do not match");
    }
}




