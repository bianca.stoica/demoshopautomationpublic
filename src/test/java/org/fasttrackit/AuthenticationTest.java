package org.fasttrackit;

import io.qameta.allure.*;
import org.fasttrackit.config.Config;
import org.fasttrackit.dataprovider.AuthenticationDataProvider;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class AuthenticationTest extends Config {
    Page page = new Page();
    Header header = new Header();
    ModalDialog modal = new ModalDialog();
    Footer footer = new Footer();

    @BeforeClass
    public void setup() {
        page.openHomePage();
    }

    @AfterMethod
    public void clean_up() {
        footer.clickToReset();
    }

    @Test(dataProvider = "validCredentials", dataProviderClass = AuthenticationDataProvider.class)
    @Owner("Bianca Stoica")
    @Description("Valid users can login with valid credentials.")
    @Severity(SeverityLevel.CRITICAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void valid_user_can_login_with_valid_credentials(ValidAccount account) {
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        assertEquals(header.getGreetingsMessage(), account.getGreetingMsg(), "Logged in with valid user, expected greetings message to be:" + account.getGreetingMsg());
    }

    @Test(dataProvider = "invalidCredentials", dataProviderClass = AuthenticationDataProvider.class)
    @Owner("Bianca Stoica")
    @Description("Invalid users can not login.")
    @Severity(SeverityLevel.CRITICAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void non_functional_authentication(InvalidAccount account) {
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        boolean errorMessageVisible = modal.errorMessageIsDisplayed();
        String errorMessage = modal.getErrorElement();
        modal.clickOnTheCloseButton();
        assertTrue(errorMessageVisible, "Error message is shown when user tries to log in with invalid credentials");
        assertEquals(errorMessage, account.getErrorMsg());
    }

    @Test(dataProvider = "validCredentials", dataProviderClass = AuthenticationDataProvider.class)
    @Owner("Bianca Stoica")
    @Description("Valid users can log out from the account.")
    @Severity(SeverityLevel.CRITICAL)
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    public void valid_user_can_log_out_from_the_account(ValidAccount account) {
        header.clickOnTheLoginButton();
        modal.typeInUsername(account.getUser());
        modal.typeInPassword(account.getPassword());
        modal.clickOnTheLoginButton();
        header.clickOnTheLogOutButton();
        assertEquals(header.getGreetingsMessage(), "Hello guest!", "Logged out valid user, expected greetings message to be 'Hello guest!'");
    }
}



